import gql from 'graphql-tag';

export const FETCH_CATEGORIES_POST = gql`
    query categories($sort: [SortCategory], $filter: FilterCategory) {
        data: categories(sortBy: $sort, filter: $filter) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                value: id
                text: name
                slug
                description
                parentId
                categoryOrder
                createdAt
                updatedAt
                deletedAt
            }
        }
    }
`;

export const CATEGORY_DETAIL = gql`
    query category($id: [ValueType]) {
        data: category(filter: { field: id, operator: eq, value: $id }) {
            id
            name
            description
            parentId
            disableId: parentId
            slug
            categoryOrder
            seoTitle
            seoDescription
            seoKeyword
            meta {
                metaKey
                metaValue
            }
            language
            languageMaster
            otherLanguages {
                id
                language
                languageMaster
            }
            createdAt
            updatedAt
        }
    }
`;