import gql from 'graphql-tag';

export const FETCH_ROLES = gql`
    query roles($page: Int, $sort: [SortRole]) {
        data: roles(page: $page, sortBy: $sort, limit: 10) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                value: id
                text: displayName
                displayName
                description
                createdAt
                updatedAt
            }
        }
    }
`;
export const FETCH_ALL_ROLE = gql`
    query roleAll($page: Int, $sort: [SortRole]) {
        data: roles(page: $page, sortBy: $sort, limit: 1000) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                displayName
                description
                createdAt
                updatedAt
            }
        }
    }
`;

export const ROLE_DETAIL = gql`
    query role($id: [ValueType]) {
        role(filter: { operator: eq, field: id, value: $id }) {
            id
            name
            displayName
            description
            isDefault
            createdAt
            updatedAt
            permissions {
                id
                name
                displayName
                description
            }
        }
    }
`;

export const FETCH_PERMISSIONS = gql`
    query permissions {
        permissions {
            id
            name
            displayName
            description
        }
    }
`;