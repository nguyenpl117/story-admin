import gql from 'graphql-tag';

export const OPTION_META = gql`
query optionMeta {
  data: options {
    metaHeadHTML
    metaHeadJSON
    metaFooterHTML
    metaFooterJSON
    themeNumberPhone
    themeFacebookMessage
    themeZaloMessage
    themeSocials
    googleMap
  }
}
`;

export const OPTION_GENERAL = gql`
query optionGeneral {
  data: options {
    siteurl
    home
    blogname
    logo
    favicon
    blogdescription
    usersCanRegister
    adminEmail
    defaultRole
    analyticsTrackingCode
    analyticsViewId
    address
    addressCity
    zipcode
    youtubeLink
  }
}
`;

export const OPTION_DISCUSSION = gql`
    query optionDiscussion {
        data: optionDiscussion {
            defaultCommentStatus
            commentOrder
            commentModeration
            commentModerationKeys
            commentBlacklistKeys
            commentMaxLinks
        }
    }
`;

export const OPTION_SOCIAL = gql`
    query optionSocial {
        data: optionSocial {
            socialLogin
            facebookLoginEnable
            facebookClientId
            facebookClientSecret
            facebookScopes
            facebookRedirectURL
            googleLoginEnable
            googleClientId
            googleClientSecret
            googleScopes
            googleRedirectURL
            githubLoginEnable
            githubClientId
            githubClientSecret
            githubScopes
            githubRedirectURL
            autoRegister
            autoRegisterDisabledMessage
            socialRegisterRole
        }
    }
`;
