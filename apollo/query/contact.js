import gql from 'graphql-tag';

export const FETCH_CONTACTS = gql`
    query contacts($page: Int, $sort: [SortContact], $filter: FilterContact) {
        data: contacts(page: $page, sortBy: $sort, filter: $filter, limit: 10) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                email
                phone
                address
                content
                subject
                status
                createdAt
                updatedAt
                deletedAt
            }
        }
    }
`;

export const CONTACT_DETAIL = gql`
    query contact($id: [ValueType]) {
        contact(filter: { field: id, value: $id, operator: eq }) {
            id
            name
            email
            phone
            address
            content
            subject
            status
            createdAt
        }
    }
`;

export const FETCH_CONTACT_REPLIES = gql`
    query contactReplies($id: [ValueType]) {
        contactReplies(
            filter: { operator: eq, value: $id, field: contactId }
            limit: 100
        ) {
            data {
                id
                message
                createdAt
            }
        }
    }
`;