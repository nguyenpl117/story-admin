import gql from 'graphql-tag';

export const PROFILE = gql`
    query profile{
        profile {
            id
            phone
            email
            name
            avatar
        }
    }
`;

export const FETCH_USERS213 = gql`
    query users($page: Int, $sort: [SortUser], $filter: FilterUser) {
        data: users(page: $page, sortBy: $sort, filter: $filter, limit: 10) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                email
                symbol: email
                website_slug: name
            }
        }
    }
`;


export const FETCH_USERS = gql`
    query users($page: Int, $sort: [SortUser], $filter: FilterUser) {
        data: users(page: $page, sortBy: $sort, filter: $filter, limit: 10) {
            to
            total
            currentPage
            perPage
            data {
                id
                phone
                name
                dob
                email
                gender
                avatar
                createdAt
                updatedAt
                roles {
                    id
                    name
                    displayName
                }
            }
        }
    }
`;
export const FETCH_USER = gql`
    query user($id: [ValueType]) {
        data: user(filter: { field: id, operator: eq, value: $id }) {
            id
            phone
            name
            dob
            email
            gender
            avatar
            createdAt
            updatedAt
            roles {
                id
                name
                displayName
            }
        }
    }
`;