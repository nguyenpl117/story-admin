import gql from 'graphql-tag';

export const FETCH_ZONES = gql`
    query shipping_zones($page: Int, $sort: [SortShippingZone], $filter: FilterShippingZone) {
        data: shipping_zones(page: $page, sortBy: $sort, filter: $filter, limit: 20) {
            from
            to
            data {
                id
                name
                zoneOrder
                location {
                    id
                    zoneId
                    code
                    type
                }
                methods {
                    id
                    zoneId
                    methodType
                    methodOrder
                    title
                    requires
                    value
                    taxStatus
                    cost
                    isEnabled
                }
            }
        }
    }
`;

export const FETCH_METHODS = gql`
    query shipping_method($id: [ValueType], $sort: [SortShippingMethod]) {
        methods: shipping_methods(
            filter: { value: $id, field: zoneId, operator: eq }
            sortBy: $sort
        ) {
            data {
                id
                zoneId
                methodType
                methodOrder
                title
                requires
                value
                taxStatus
                cost
                isEnabled
            }
        }
    }
`;

export const FETCH_ZONE = gql`
    query shipping_zone($id: [ValueType]) {
        data: shipping_zone(filter: { value: $id, field: id, operator: eq }) {
            id
            name
            zoneOrder
            location {
                id
                zoneId
                code
                type
            }
            methods {
                id
                zoneId
                methodType
                methodOrder
                title
                requires
                value
                taxStatus
                cost
                isEnabled
            }
        }
    }
`;