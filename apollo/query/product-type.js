import gql from 'graphql-tag';

export const FETCH_PRODUCT_TYPES = gql`
    query productTypes($sort: [SortProductType], $filter: FilterProductType) {
        data: productTypes(sortBy: $sort, filter: $filter) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                value: id
                text: name
                slug
                description
                parentId
                categoryOrder
                createdAt
                updatedAt
                deletedAt
            }
        }
    }
`;

export const PRODUCT_TYPE_DETAIL = gql`
    query productType($id: [ValueType]) {
        data: productType(filter: { field: id, operator: eq, value: $id }) {
            id
            name
            description
            parentId
            disableId: parentId
            slug
            categoryOrder
            seoTitle
            seoDescription
            seoKeyword
            meta {
                metaKey
                metaValue
            }
            language
            languageMaster
            otherLanguages {
                id
                language
                languageMaster
            }
            createdAt
            updatedAt
        }
    }
`;