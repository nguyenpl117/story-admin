'use strict';
import gql from 'graphql-tag';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 8/4/2019
 * Time: 2:34 PM
 */

export const OPTION_EMAIL = gql`
    query optionEmail{
        optionEmail{
            SMTPHost
            SMTPEncryption
            SMTPPort
            SMTPUsername
            SMTPPassword
            SMTPSenderName
            SMTPSenderEmail
        }
    }
`;