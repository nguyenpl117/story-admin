import gql from 'graphql-tag';

export const FETCH_PAGE = gql`
    query pages($page: Int, $sort: [SortPage], $filter: FilterPage) {
        data: pages(page: $page, sortBy: $sort, limit: 10, filter: $filter) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                avatar
                author {
                    id
                    name
                }
                postStatus
                slug
                description
                content
                publishedAt
                createdAt
                updatedAt
                deletedAt
                meta {
                    metaKey
                    metaValue
                }
            }
        }
    }
`;

export const PAGE_DETAIL = gql`
    query page($id: [ValueType]) {
        page(filter: { field: id, value: $id, operator: eq }) {
            id
            name
            avatar
            description
            content
            postStatus
            postPassword
            commentStatus
            createdAt
            meta{
                metaKey
                metaValue
            }
        }
    }
`;