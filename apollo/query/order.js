import gql from 'graphql-tag';

export const FETCH_ORDER = gql`
    query order($id: [ValueType]) {
        order(filter: { field: id, operator: eq, value: $id }) {
            id
            code
            orderStatusId
            status {
                id
                color
                name
            }
            shipping {
                id
                orderId
                price
                name
                methodId
                methodType
            }
            billingAddress {
                id
                orderId
                country
                company
                city
                address
                address2
                phone
                email
                posCode
                name
                paymentMethod
            }
            shippingAddress {
                id
                orderId
                country
                company
                city
                address
                address2
                phone
                posCode
                name
                note
            }
            items {
                id
                orderId
                productBranchId
                sku
                code
                name
                image
                price
                quantity
                total
                discount
                discountType
                tax
                reward
                type
            }
            totalOrigin
            total
            discount
            discountType
            taxValue
            customerId
            user: customer {
                id
                name
                email
                phone
                avatar
            }
            customerGroupId
            ip
            forwardedIp
            userAgent
            createdAt
            updatedAt
        }
    }
`;

export const FETCH_ORDERS = gql`
    query orders($page: Int, $sort: [SortOrder], $filter: FilterOrder) {
        data: orders(page: $page, sortBy: $sort, filter: $filter, limit: 10) {
            to
            total
            currentPage
            perPage
            data {
                id
                code
                orderStatusId
                status {
                    id
                    color
                    name
                }
                customerId
                customer {
                    id
                    name
                }
                shipping {
                    id
                    orderId
                    price
                    name
                    methodId
                    methodType
                }
                billingAddress {
                    id
                    orderId
                    country
                }
                shippingAddress {
                    id
                    orderId
                    country
                    company
                    city
                    address
                    address2
                    phone
                    posCode
                    name
                    note
                }
                items {
                    id
                    orderId
                    productBranchId
                    price
                }
                totalOrigin
                total
                discount
                customerGroupId
                ip
                forwardedIp
                userAgent
                createdAt
                updatedAt
                deletedAt
            }
        }
    }
`;

export const FETCH_ORDER_STATUS = gql`
    query orderStatus{
        data: orderStatus{
            data{
                id
                color
                name

            }
        }
    }`;