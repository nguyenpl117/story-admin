import gql from 'graphql-tag';

export const FETCH_POST = gql`
    query posts($page: Int, $sort: [SortPost], $filter: FilterPost) {
        data: posts(page: $page, sortBy: $sort, filter: $filter, limit: 10) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                isFeatured
                format
                avatar
                thumbnailId
                thumbnail {
                    id
                    guid
                    data
                }
                postStatus
                categories {
                    id
                    name
                }
                tags {
                    id
                    text: name
                }
                author {
                    id
                    name
                }
                meta {
                    id
                    metaKey
                    metaValue
                }
                slug
                description
                content
                publishedAt
                createdAt
                updatedAt
                deletedAt
            }
        }
    }
`;

export const POST_DETAIL = gql`
    query post($id: [ValueType]) {
        post(filter: { field: id, value: $id, operator: eq }) {
            id
            name
            isFeatured
            format
            avatar
            thumbnailId
            thumbnail {
                id
                guid
                data
            }
            description
            content
            categories {
                id
                name
            }
            tags {
                id
                text: name
                slug
                createdAt
            }
            meta {
                id
                metaKey
                metaValue
            }
            postStatus
            postPassword
            commentStatus
            createdAt
        }
    }
`;