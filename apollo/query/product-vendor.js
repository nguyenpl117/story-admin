import gql from 'graphql-tag';

export const FETCH_PRODUCT_VENDOR = gql`
    query productVendor {
        data: productVendors {
            data {
                id
                name
                value: id
                text: name
            }
        }
    }
`;