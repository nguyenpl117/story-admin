import gql from 'graphql-tag';

export const FETCH_COMMENTS = gql`
    query comments($page: Int, $sort: [SortComment]){
        data: comments(page: $page, sortBy: $sort, limit: 10){
            to
            total
            currentPage
            perPage
            data{
                id
                body
                status
                author{
                    id
                    name
                    email
                }
                responseTo{
                    id
                    name
                    slug
                }
                commentableId
                commentableType
                publishedAt
                createdAt
                updatedAt
            }
        }
    }
`;

export const COMMENT_DETAIL = gql`
    query comment($id: [ValueType]) {
        comment(filter: {field: id, value: $id, operator: eq}) {
            id
            body
            status
            author {
                id
                name
            }
            responseTo{
                id
                name
                slug
            }
            commentableId
            commentableType
            publishedAt
            createdAt
            updatedAt
        }
    }
`;