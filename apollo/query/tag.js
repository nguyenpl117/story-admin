import gql from 'graphql-tag';

export const FETCH_TAGS = gql`
    query tags($sort: [SortTag], $page: Int, $perPage: Int, $filter: FilterTag) {
        data: tags(sortBy: $sort, page: $page, limit: $perPage, filter: $filter) {
            to
            total
            currentPage
            perPage
            data {
                id
                text: name
                value: id
                slug
                createdAt
            }
        }
    }
`;

export const TAG_DETAIL = gql`
    query tag($id: [ValueType]) {
        data: tag(filter: { field: id, operator: eq, value: $id }) {
            id
            name
            slug
            createdAt
            updatedAt
        }
    }
`;