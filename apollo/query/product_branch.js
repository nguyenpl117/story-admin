import gql from 'graphql-tag';

export const FETCH_PRODUCT_BRANCH_DETAIL = gql`
    query product_branch($id: [ValueType]) {
        data: product_branch(filter: { value: $id, field: id, operator: eq }) {
            id
            branchCount
            inventory {
                id
                quantity
            }
            sku
            code
            fullname
            requiresShipping
            branchCount
            price
            priceSale
            weight
            length
            width
            height
            attributes {
                id
                group: attributeGroup {
                    id
                    name
                }
                attribute {
                    id
                    name
                }
            }
            master {
                id
                name
            }
            branches {
                id
                attributes {
                    id
                    group: attributeGroup {
                        id
                        name
                    }
                    attribute {
                        id
                        name
                    }
                }
            }
            images {
                id
                productMasterId
                image
            }
            createdAt
        }
    }
`;