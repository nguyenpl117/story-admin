import gql from 'graphql-tag';

export const FETCH_BACKUPS = gql`
    query backups($page: Int, $sort: [SortBackup], $filter: FilterBackup) {
        data: backups(page: $page, sortBy: $sort, filter: $filter, limit: 10) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                description
                size
                createdAt
                updatedAt
            }
        }
    }
`;
