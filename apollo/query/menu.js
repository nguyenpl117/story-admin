import gql from 'graphql-tag';

export const FETCH_MENUS = gql`
    query menus($page: Int, $sort: [SortMenu], $filter: FilterMenu) {
        data: menus(page: $page, sortBy: $sort, filter: $filter,limit: 10) {
            to
            total
            currentPage
            perPage
            data {
                id
                name
                alias
                description
                status
                menuItems {
                    id
                    menuId
                    title
                    link
                    icon
                    className
                    target
                    image
                    objectType
                    objectId
                    objectItem {
                        id
                        name
                        slug
                    }
                    parentId
                    sort
                    createdAt
                    updatedAt
                }
                language
                languageMaster
                otherLanguages {
                    id
                    language
                    languageMaster
                }
                createdAt
                updatedAt
                deletedAt
            }
        }
    }
`;

export const FETCH_MENU = gql`
    query menu($id: [ValueType]) {
        menu(filter: { field: id, value: $id, operator: eq }) {
            id
            name
            headerNavigation {
                id
                name
            }
            mainNavigation {
                id
                name
            }
            footerNavigation {
                id
                name
            }
            automanticallyMenu
            alias
            description
            status
            menuItems {
                id
                menuId
                title
                link
                icon
                className
                target
                image
                objectType
                objectId
                objectItem {
                    id
                    name
                    slug
                }
                parentId
                sort
                createdAt
                updatedAt
            }
            language
            languageMaster
            otherLanguages {
                id
                language
                languageMaster
            }
            createdAt
            updatedAt
            deletedAt
        }
    }
`;