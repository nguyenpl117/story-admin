import gql from 'graphql-tag';

export const FETCH_LANGUAGES = gql`
    query languages($page: Int, $sort: [SortLanguage]) {
        data: languages(page: $page, sortBy: $sort, limit: 10) {
            from
            to
            perPage
            currentPage
            total
            data {
                id
                name
                locale
                code
                direction
                flag
                position
                default
                createdAt
                updatedAt
                deletedAt
            }
        }
    }
`;

export const OPTION_LANGUAGE = gql`
    query optionLanguage{
        optionLanguage{
            defaultLanguage
            hideDefaultLanguage
            displayLanguage
            hideLanguage
            showItemDefaultLanguage
        }
    }
`;