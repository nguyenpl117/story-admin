import gql from 'graphql-tag';

export const FETCH_PRODUCT_BRANCH = gql`
    query product_branches(
        $filter: FilterProductBranch
        $sort: [SortProductBranch]
        $perPage: Int = 20
        $page: Int = 1
    ) {
        data: product_branches(
            filter: $filter
            sortBy: $sort
            limit: $perPage
            page: $page
        ) {
            data {
                id
                branchCount
                inventory {
                    id
                    quantity
                    adjust: quantity
                }
                sku
                code
                fullname
                branchCount
                attributes {
                    id
                    attributeGroup {
                        id
                        name
                    }
                    attribute {
                        id
                        name
                    }
                }
                master {
                    id
                    name
                    kind
                }
                images {
                    id
                    productMasterId
                    thumbnailId
                    image
                    thumbnail {
                        id
                        guid
                        data
                    }
                }
                createdAt
            }
            from
            to
            perPage
            currentPage
            total
        }
    }
`;

export const FETCH_PRODUCT = gql`
    query product_master_list(
        $filter: FilterProductMaster
        $sort: [SortProductMaster]
        $perPage: Int = 20
        $page: Int = 1
    ) {
        data: product_master_list(
            filter: $filter
            sortBy: $sort
            limit: $perPage
            page: $page
        ) {
            data {
                id
                name
                kind
                avatar
                content
                inventory
                productTypeId
                branchCount
                isFeatured
                tags {
                    id
                    name
                }
                categories {
                    id
                    name
                }
                productVendorId
                vendor {
                    id
                    name
                }
                branches {
                    id
                    sku
                    code
                    fullname
                    branchCount
                    price
                    master {
                        id
                        name
                    }
                    images {
                        id
                        productMasterId
                        thumbnailId
                        image
                        thumbnail {
                            id
                            guid
                            data
                        }
                    }
                }
                attributeGroups {
                    id
                }
                createdAt
            }
            from
            to
            perPage
            currentPage
            total
        }
    }
`;

export const PRODUCT_MASTER = gql`
    query product_master($id: [ValueType]) {
        master: product_master(filter: { field: id, value: $id, operator: eq }) {
            id
            name
            description
            content
            avatar
            imageType
        }
    }
`;

export const PRODUCT_DETAIL = gql`
    query product_master($id: [ValueType]) {
        data: product_master(filter: { field: id, value: $id, operator: eq }) {
            id
            name
            description
            content
            avatar
            imageType
            meta {
                id
                metaKey
                metaValue
            }
            categories {
                id
                name
            }
            tags {
                id
                name
                text: name
            }
            productVendorId
            vendor {
                id
                name
            }
            branches {
                id
                sku
                code
                fullname
                requiresShipping
                inventory {
                    id
                    quantity
                }
                master {
                    id
                    name
                }
                attributes {
                    id
                    group: attributeGroup {
                        id
                        name
                    }
                    attribute {
                        id
                        name
                    }
                }
                images {
                    id
                    image
                    thumbnailId
                    thumbnail {
                        id
                        guid
                        data
                    }
                }
                price
                priceSale
                weight
                length
                width
                height
            }
        }
    }
`;

export const ALL_ATTRIBUTE = gql`
    query product_all_attribute($id: ID_CRYPTO){
        productAllAttribute: product_all_attribute(productMasterId: $id){
            id
            name
            attributes{
                id
                name
            }
        }
    }
`;