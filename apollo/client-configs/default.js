import { BatchHttpLink } from 'apollo-link-batch-http';
import { createPersistedQueryLink } from 'apollo-link-persisted-queries';

export default function(context) {

    let uri = context.env.GRAPHQL_URL;

    const Persisted = (args) => {
        if ( context.env.NODE_ENV === 'production' ) {
            return createPersistedQueryLink({}).concat(args);
        }
        return args;
    };

    const link = Persisted(
        new BatchHttpLink({
            uri, batchMax: process.server ? 20 : 10
            /*fetch: process.server ? async function() {
                if (context.env.NODE_ENV !== 'production') {
                    console.log('default', arguments[1].body);
                    const t = Date.now();
                    return fetch.apply(this, arguments).then(data => {
                        console.log('time', Date.now() - t);
                        return data;
                    });
                }
                return fetch.apply(this, arguments);
            } : fetch*/
        })
    );

    return {
        link: link,
        ssrForceFetchDelay: 10,
        defaultHttpLink: false,
        // ssr: true,
        httpEndpoint: context.env.GRAPHQL_URL,
        // wsEndpoint: uri.replace('http', 'ws'),
        tokenName: context.env.AUTH_TOKEN || 'apollo-token', // optional
        httpLinkOptions: {
            headers: {
                'x-token': 'hash',
                'x-auth-token': 'hash',
                'x-refresh-token': 'hash'
            }
        }
    };
}
