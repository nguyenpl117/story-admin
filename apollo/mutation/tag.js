import gql from 'graphql-tag';

export const CREATE_TAG_POST = gql`
    mutation tagCreate($name: String, $slug: String) {
        tagCreate(name: $name, slug: $slug) {
            id
            name
        }
    }
`;

export const UPDATE_TAG_POST = gql`
    mutation tagUpdate($id: ID_CRYPTO, $name: String, $slug: String) {
        tagUpdate(id: $id, name: $name, slug: $slug) {
            id
            name
            slug
            createdAt
            updatedAt
        }
    }
`;

export const DELETE_TAG = gql`
    mutation tagDelete($id: [ID_CRYPTO]) {
        tagDelete(id: $id) {
            status
            message
            data
        }
    }
`;