'use strict';
import gql from 'graphql-tag';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 8/4/2019
 * Time: 2:43 PM
 */
export const OPTION_EMAIL_UPDATE = gql`
    mutation optionEmailUpdate(
        $SMTPHost: String
        $SMTPEncryption: String
        $SMTPPort: String
        $SMTPUsername: String
        $SMTPPassword: String
        $SMTPSenderName: String
        $SMTPSenderEmail: String
    ) {
        optionEmailUpdate(
            SMTPHost: $SMTPHost
            SMTPEncryption: $SMTPEncryption
            SMTPPort: $SMTPPort
            SMTPUsername: $SMTPUsername
            SMTPPassword: $SMTPPassword
            SMTPSenderName: $SMTPSenderName
            SMTPSenderEmail: $SMTPSenderEmail
        ) {
            SMTPHost
            SMTPEncryption
            SMTPPort
            SMTPUsername
            SMTPPassword
            SMTPSenderName
            SMTPSenderEmail
        }
    }
`;

export const TEST_EMAIL = gql`
    mutation testEmail($to: String, $from: String, $message: String) {
        testEmail(to: $to, from: $from, message: $message)
    }
`;