import gql from 'graphql-tag';

export const CREATE_CATEGORY_POST = gql`
    mutation categoryCreate(
        $name: String
        $slug: String
        $description: String
        $parentId: ID_CRYPTO
        $categoryOrder: Int
        $seoTitle: String
        $seoDescription: String
        $seoKeyword: String
        $meta: [MetaInput]
    ) {
        categoryCreate(
            name: $name
            slug: $slug
            description: $description
            parentId: $parentId
            categoryOrder: $categoryOrder
            seoTitle: $seoTitle
            seoDescription: $seoDescription
            seoKeyword: $seoKeyword
            meta: $meta
        ) {
            id
            name
        }
    }
`;

export const UPDATE_CATEGORY_POST = gql`
    mutation categoryUpdate(
        $id: ID_CRYPTO
        $name: String
        $slug: String
        $description: String
        $parentId: ID_CRYPTO
        $categoryOrder: Int
        $seoTitle: String
        $seoDescription: String
        $seoKeyword: String
        $meta: [MetaInput]
    ) {
        categoryUpdate(
            id: $id
            name: $name
            slug: $slug
            description: $description
            parentId: $parentId
            categoryOrder: $categoryOrder
            seoTitle: $seoTitle
            seoDescription: $seoDescription
            seoKeyword: $seoKeyword
            meta: $meta
        ) {
            id
            name
            description
            parentId
            disableId: parentId
            slug
            categoryOrder
            seoTitle
            seoDescription
            seoKeyword
            meta {
                metaKey
                metaValue
            }
            language
            languageMaster
            otherLanguages {
                id
                language
                languageMaster
            }
            createdAt
            updatedAt
        }
    }
`;

export const DELETE_CATEGORY = gql`
    mutation categoryDelete($id: [ID_CRYPTO]) {
        categoryDelete(id: $id) {
            status
            message
            data
        }
    }
`;