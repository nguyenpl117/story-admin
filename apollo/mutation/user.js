import gql from 'graphql-tag';

export const CREATE_USER = gql`
    mutation createUser(
        $name: String
        $avatar: String
        $gender: GenderEnum
        $phone: String
        $password: String
        $email: String
        $roles: [ID_CRYPTO]
    ) {
        userCreate(
            name: $name
            email: $email
            phone: $phone
            password: $password
            avatar: $avatar
            gender: $gender
            roles: $roles
        ) {
            id
            phone
            name
        }
    }
`;

export const UPDATE_USER = gql`
    mutation updateUser(
        $id: ID_CRYPTO
        $avatar: String
        $name: String
        $phone: String
        $gender: GenderEnum
        $dob: Timestamp
        $roles: [ID_CRYPTO]
        $password: String
    ) {
        userUpdate(
            id: $id
            avatar: $avatar
            name: $name
            phone: $phone
            gender: $gender
            dob: $dob
            roles: $roles
            password: $password
        ) {
            id
            phone
            name
            dob
            email
            gender
            avatar
            createdAt
            updatedAt
            roles {
                id
                name
                displayName
            }
        }
    }
`;

export const DELETE_USER = gql`
    mutation deleteUser($id: [ID_CRYPTO]) {
        userDelete(id: $id) {
            status
            message
            data
        }
    }
`;