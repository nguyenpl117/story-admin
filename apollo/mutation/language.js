import gql from 'graphql-tag';

export const LANGUAGE_CONFIG_UPDATE = gql`
    mutation languageConfigUpdate(
        $defaultLanguage: ID_CRYPTO
        $hideDefaultLanguage: Boolean
        $displayLanguage: Int
        $hideLanguage: [ID_CRYPTO]
        $showItemDefaultLanguage: Boolean
    ) {
        languageConfigUpdate(
            defaultLanguage: $defaultLanguage
            hideDefaultLanguage: $hideDefaultLanguage
            displayLanguage: $displayLanguage
            hideLanguage: $hideLanguage
            showItemDefaultLanguage: $showItemDefaultLanguage
        ) {
            defaultLanguage
            hideDefaultLanguage
            displayLanguage
            hideLanguage
            showItemDefaultLanguage
        }
    }
`;

export const CREATE_LANGUAGE = gql`
    mutation createLanguage(
        $name: String
        $locale: String
        $code: String
        $direction: Int
        $flag: String
        $position: Int
        $default: Boolean
        $status: LanguageStatusEnum
    ) {
        languageCreate(
            name: $name
            locale: $locale
            code: $code
            direction: $direction
            flag: $flag
            position: $position
            default: $default
            status: $status
        ) {
            id
            name
            locale
            code
            direction
            flag
            position
            default
            createdAt
            updatedAt
        }
    }
`;

export const UPDATE_LANGUAGE = gql`
    mutation updateLanguage(
        $id: ID_CRYPTO
        $name: String
        $locale: String
        $code: String
        $direction: Int
        $flag: String
        $position: Int
        $default: Boolean
        $status: LanguageStatusEnum
    ) {
        languageUpdate(
            id: $id
            name: $name
            locale: $locale
            code: $code
            direction: $direction
            flag: $flag
            position: $position
            default: $default
            status: $status
        ) {
            id
            name
            locale
            code
            direction
            flag
            position
            default
            createdAt
            updatedAt
        }
    }
`;

export const DELETE_LANGUAGE = gql`
    mutation deleteLanguage($id: [ID_CRYPTO]) {
        languageDelete(id: $id) {
            status
            message
            data
        }
    }
`;