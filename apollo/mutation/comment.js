import gql from 'graphql-tag';

export const COMMENT_POST_UPDATE = gql`
    mutation commentPostUpdate(
        $id: String
        $body: String
        $status: CommentStatusEnum
    ) {
        commentPostUpdate(id: $id, body: $body, status: $status) {
            id
            status
            updatedAt
            deletedAt
        }
    }
`;

export const COMMENT_PRODUCT_UPDATE = gql`
    mutation commentProductUpdate(
        $id: String
        $body: String
        $status: CommentStatusEnum
    ) {
        commentProductUpdate(id: $id, body: $body, status: $status) {
            id
            status
            updatedAt
            deletedAt
        }
    }
`;

export const DELETE_COMMENT = gql`
    mutation deleteComment($id: [ID_CRYPTO]) {
        commentDelete(id: $id) {
            status
            message
            data
        }
    }
`;
