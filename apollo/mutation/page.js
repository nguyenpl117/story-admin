import gql from 'graphql-tag';

export const CREATE_PAGE = gql`
    mutation pageCreate(
        $name: String
        $avatar: String
        $postStatus: PostStatus = publish
        $postPassword: String
        $commentStatus: PostCommentStatus = open
        $slug: String
        $description: String
        $content: String
        $tags: [String]
        $categories: [ID_CRYPTO] = [false]
        $seoTitle: String
        $seoDescription: String
        $seoKeyword: String
    ) {
        pageCreate(
            name: $name
            avatar: $avatar
            postStatus: $postStatus
            postPassword: $postPassword
            commentStatus: $commentStatus
            slug: $slug
            description: $description
            content: $content
            tags: $tags
            categories: $categories
            seoTitle: $seoTitle
            seoDescription: $seoDescription
            seoKeyword: $seoKeyword
        ) {
            id
            name
        }
    }
`;

export const UPDATE_PAGE = gql`
    mutation pageUpdate(
        $id: ID_CRYPTO
        $name: String
        $postStatus: PostStatus
        $postPassword: String
        $commentStatus: PostCommentStatus
        $avatar: String
        $slug: String
        $description: String
        $content: String
        $tags: [String]
        $categories: [ID_CRYPTO]
        $seoTitle: String
        $seoDescription: String
        $seoKeyword: String
    ) {
        pageUpdate(
            id: $id
            name: $name
            postStatus: $postStatus
            postPassword: $postPassword
            commentStatus: $commentStatus
            avatar: $avatar
            slug: $slug
            description: $description
            content: $content
            tags: $tags
            categories: $categories
            seoTitle: $seoTitle
            seoDescription: $seoDescription
            seoKeyword: $seoKeyword
        ) {
            id
            name
        }
    }
`;

export const DELETE_PAGE = gql`
    mutation pageDelete($id: [ID_CRYPTO]) {
        pageDelete(id: $id) {
            status
            message
            data
        }
    }
`;