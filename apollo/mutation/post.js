import gql from 'graphql-tag';

export const CREATE_POST = gql`
    mutation postCreate(
        $name: String
        $format: Int
        $avatar: String
        $thumbnailId: ID_CRYPTO
        $postStatus: PostStatus = publish
        $postPassword: String
        $commentStatus: PostCommentStatus = open
        $slug: String
        $description: Html
        $content: Html
        $tags: [String]
        $categories: [ID_CRYPTO] = [false]
        $seoTitle: String
        $seoDescription: String
        $seoKeyword: String
        $meta: [MetaInput]
    ) {
        postCreate(
            name: $name
            format: $format
            avatar: $avatar
            thumbnailId: $thumbnailId
            postStatus: $postStatus
            postPassword: $postPassword
            commentStatus: $commentStatus
            slug: $slug
            description: $description
            content: $content
            tags: $tags
            categories: $categories
            seoTitle: $seoTitle
            seoDescription: $seoDescription
            seoKeyword: $seoKeyword
            meta: $meta
        ) {
            id
            name
        }
    }
`;

export const UPDATE_POST = gql`
    mutation postUpdate(
        $id: ID_CRYPTO
        $name: String
        $format: Int
        $postStatus: PostStatus
        $postPassword: String
        $commentStatus: PostCommentStatus
        $avatar: String
        $thumbnailId: ID_CRYPTO
        $slug: String
        $description: Html
        $content: Html
        $tags: [String]
        $categories: [ID_CRYPTO]
        $seoTitle: String
        $seoDescription: String
        $seoKeyword: String
        $meta: [MetaInput]
    ) {
        postUpdate(
            id: $id
            name: $name
            format: $format
            postStatus: $postStatus
            postPassword: $postPassword
            commentStatus: $commentStatus
            avatar: $avatar
            thumbnailId: $thumbnailId
            slug: $slug
            description: $description
            content: $content
            tags: $tags
            categories: $categories
            seoTitle: $seoTitle
            seoDescription: $seoDescription
            seoKeyword: $seoKeyword
            meta: $meta
        ) {
            id
            name
            avatar
            thumbnailId
            thumbnail {
                id
                guid
                data
            }
            description
            content
            categories {
                id
                name
            }
            tags {
                id
                text: name
                slug
                createdAt
            }
            meta {
                id
                metaKey
                metaValue
            }
            postStatus
            postPassword
            commentStatus
            createdAt
        }
    }
`;

export const DELETE_POST = gql`
    mutation postDelete($id: [ID_CRYPTO]!) {
        postDelete(id: $id) {
            status
            message
            data
        }
    }
`;

export const CHANGE_FEATURED_POST = gql`
    mutation postChangeFeature($id: ID_CRYPTO, $isFeatured: Boolean) {
        postChangeFeature(id: $id, isFeatured: $isFeatured) {
            id
            isFeatured
        }
    }
`;