import gql from 'graphql-tag';

export const CREATE_PRODUCT = gql`
    mutation productCreate(
        $name: String
        $avatar: String
        $imageType: String
        $description: Html
        $content: Html
        $tags: [String]
        $categories: [ID_CRYPTO]
        $branches: [ProductBranchInput]
        $meta: [MetaInput]
    ) {
        productCreate(
            name: $name
            avatar: $avatar
            imageType: $imageType
            description: $description
            content: $content
            tags: $tags
            categories: $categories
            branches: $branches
            meta: $meta
        ) {
            id
            name
            content
            inventory
            productTypeId
            branchCount
            branches {
                id
                sku
                code
                fullname
            }
            attributeGroups {
                id
                sku
            }
            createdAt
        }
    }
`;

export const UPDATE_PRODUCT = gql`
    mutation productUpdate(
        $id: ID_CRYPTO
        $name: String
        $avatar: String
        $imageType: String
        $description: Html
        $content: Html
        $productVendorId: ID_CRYPTO
        $tags: [String]
        $categories: [ID_CRYPTO]
        $branches: [ProductBranchInput]
        $meta: [MetaInput]
        $kind: ProductKind
    ) {
        productUpdate(
            id: $id
            name: $name
            avatar: $avatar
            imageType: $imageType
            description: $description
            content: $content
            productVendorId: $productVendorId
            tags: $tags
            categories: $categories
            branches: $branches
            meta: $meta
            kind: $kind
        ) {
            id
            name
            description
            content
            avatar
            imageType
            meta {
                id
                metaKey
                metaValue
            }
            categories {
                id
                name
            }
            tags {
                id
                name
                text: name
            }
            productVendorId
            vendor{
                id
                name
            }
            branches{
                id
                sku
                code
                fullname
                requiresShipping
                inventory{
                    id
                    quantity
                }
                master{
                    id
                    name
                }
                attributes{
                    id
                    group: attributeGroup{
                        id
                        name
                    }
                    attribute{
                        id
                        name
                    }
                }
                images{
                    id
                    thumbnailId
                    image
                    thumbnail {
                        id
                        guid
                        data
                    }
                }
                price
                priceSale
                weight
                length
                width
                height
            }
        }
    }
`;

export const DELETE_PRODUCT_BRANCH = gql`
    mutation productBranchDelete($id: [ID_CRYPTO]) {
        productBranchDelete(id: $id) {
            status
            message
            data
        }
    }
`;

export const DELETE_PRODUCT_MASTER = gql`
    mutation productDelete($id: [ID_CRYPTO]) {
        productDelete(id: $id) {
            status
            message
            data
        }
    }
`;

export const INVENTORY_ADJUST_QUANTITY = gql`
    mutation inventory_adjust_quantity(
        $productBranchId: ID_CRYPTO
        $quantity: Float
    ) {
        inventory_adjust_quantity(
            productBranchId: $productBranchId
            quantity: $quantity
        ) {
            id
            quantity
        }
    }
`;

export const CHANGE_FEATURED = gql`
    mutation productChangeFeature($id: ID_CRYPTO, $isFeatured: Boolean) {
        productChangeFeature(id: $id, isFeatured: $isFeatured) {
            id
            isFeatured
        }
    }
`;