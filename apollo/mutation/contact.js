import gql from 'graphql-tag';

export const CREATE_CONTACT_REPLY = gql`
    mutation contactReplyCreate($message: String, $contactId: ID_CRYPTO) {
        contactReplyCreate(message: $message, contactId: $contactId) {
            id
            message
            createdAt
        }
    }
`;

export const UPDATE_CONTACT = gql`
    mutation contactUpdate($id: ID_CRYPTO, $status: String) {
        contactUpdate(id: $id, status: $status) {
            id
            name
            email
            phone
            address
            content
            subject
            status
            createdAt
        }
    }
`;

export const DELETE_CONTACT = gql`
    mutation contactDelete($id: [ID_CRYPTO]) {
        contactDelete(id: $id) {
            status
            message
            data
        }
    }
`;