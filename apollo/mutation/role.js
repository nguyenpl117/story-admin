import gql from 'graphql-tag';

export const DELETE_ROLE = gql`
    mutation deleteRoles($id: [ID_CRYPTO]) {
        roleDelete(id: $id) {
            status
            message
            data
        }
    }
`;

export const CREATE_ROLE = gql`
    mutation createRole(
        $name: String
        $displayName: String
        $description: String
        $permissions: [String]
        $isDefault: Boolean
    ) {
        roleCreate(
            name: $name
            displayName: $displayName
            description: $description
            permissions: $permissions
            isDefault: $isDefault
        ) {
            id
            name
            displayName
            description
        }
    }
`;

export const UPDATE_ROLE = gql`
    mutation roleUpdate(
        $id: ID_CRYPTO
        $displayName: String
        $description: String
        $permissions: [String]
        $isDefault: Boolean
    ) {
        roleUpdate(
            id: $id
            displayName: $displayName
            description: $description
            permissions: $permissions
            isDefault: $isDefault
        ) {
            id
            name
            displayName
            description
            isDefault
            createdAt
            updatedAt
            permissions {
                id
                name
                displayName
                description
            }
        }
    }

`;