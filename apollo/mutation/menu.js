import gql from 'graphql-tag';

export const CREATE_MENU = gql`
    mutation menuCreate(
        $name: String
        $language: ID_CRYPTO
        $languageMaster: ID_CRYPTO
        $status: MenuStatusEnum
        $menuItems: [MenuItemInput]
    ) {
        menuCreate(
            name: $name
            language: $language
            languageMaster: $languageMaster
            status: $status
            menuItems: $menuItems
        ) {
            id
            name
            alias
            description
        }
    }
`;

export const UPDATE_MENU = gql`
    mutation menuUpdate(
        $id: ID_CRYPTO
        $name: String
        $status: MenuStatusEnum
        $headerNavigation: ID_CRYPTO
        $mainNavigation: ID_CRYPTO
        $footerNavigation: ID_CRYPTO
        $automanticallyMenu: Boolean
        $menuItems: [MenuItemInput]
    ) {
        menuUpdate(
            id: $id
            name: $name
            status: $status
            headerNavigation: $headerNavigation
            mainNavigation: $mainNavigation
            footerNavigation: $footerNavigation
            automanticallyMenu: $automanticallyMenu
            menuItems: $menuItems
        ) {
            id
            name
            headerNavigation {
                id
                name
            }
            mainNavigation {
                id
                name
            }
            footerNavigation {
                id
                name
            }
            automanticallyMenu
            alias
            description
            status
            menuItems {
                id
                menuId
                title
                link
                icon
                className
                target
                image
                objectType
                objectId
                objectItem {
                    id
                    name
                    slug
                }
                parentId
                sort
                createdAt
                updatedAt
            }
            language
            languageMaster
            otherLanguages {
                id
                language
                languageMaster
            }
            createdAt
            updatedAt
            deletedAt
        }
    }
`;

export const DELETE_MENU = gql`
    mutation menuDelete($id: [ID_CRYPTO]) {
        menuDelete(id: $id) {
            status
            message
            data
        }
    }
`;