import gql from 'graphql-tag';

export const CREATE_PRODUCT_TYPE = gql`
    mutation productTypeCreate(
        $name: String
        $slug: String
        $description: String
        $parentId: ID_CRYPTO
        $categoryOrder: Int
        $seoTitle: String
        $seoDescription: String
        $seoKeyword: String
        $meta: [MetaInput]
    ) {
        productTypeCreate(
            name: $name
            slug: $slug
            description: $description
            parentId: $parentId
            categoryOrder: $categoryOrder
            seoTitle: $seoTitle
            seoDescription: $seoDescription
            seoKeyword: $seoKeyword
            meta: $meta
        ) {
            id
            name
        }
    }
`;

export const UPDATE_PRODUCT_TYPE = gql`
    mutation productTypeUpdate(
        $id: ID_CRYPTO
        $name: String
        $slug: String
        $description: String
        $parentId: ID_CRYPTO
        $categoryOrder: Int
        $seoTitle: String
        $seoDescription: String
        $seoKeyword: String
        $meta: [MetaInput]
    ) {
        productTypeUpdate(
            id: $id
            name: $name
            slug: $slug
            description: $description
            parentId: $parentId
            categoryOrder: $categoryOrder
            seoTitle: $seoTitle
            seoDescription: $seoDescription
            seoKeyword: $seoKeyword
            meta: $meta
        ) {
            id
            name
            description
            parentId
            disableId: parentId
            slug
            categoryOrder
            seoTitle
            seoDescription
            seoKeyword
            meta {
                metaKey
                metaValue
            }
            language
            languageMaster
            otherLanguages {
                id
                language
                languageMaster
            }
            createdAt
            updatedAt
        }
    }
`;

export const DELETE_PRODUCT_TYPE = gql`
    mutation productTypeDelete($id: [ID_CRYPTO]) {
        productTypeDelete(id: $id) {
            status
            message
            data
        }
    }
`;