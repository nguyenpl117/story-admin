import gql from 'graphql-tag';

export const CREATE_ORDER = gql`
    mutation orderCreate(
        $orderStatusId: ID_CRYPTO
        $taxValue: Float
        $discount: Float
        $discountType: Float
        $customerId: ID_CRYPTO
        $customerGroupId: ID_CRYPTO
        $items: [OrderItemInput]
        $shipping: OrderShippingInput
        $billingAddress: OrderBillingAddressInput
        $shippingAddress: OrderShippingAddressInput
    ) {
        orderCreate(
            orderStatusId: $orderStatusId
            taxValue: $taxValue
            discount: $discount
            discountType: $discountType
            customerId: $customerId
            customerGroupId: $customerGroupId
            items: $items
            shipping: $shipping
            billingAddress: $billingAddress
            shippingAddress: $shippingAddress
        ) {
            id
            code
        }
    }
`;

export const UPDATE_ORDER = gql`
    mutation orderUpdate(
        $id: ID_CRYPTO
        $orderStatusId: ID_CRYPTO
        $taxValue: Float
        $discount: Float
        $discountType: Float
        $customerId: ID_CRYPTO
        $customerGroupId: ID_CRYPTO
        $items: [OrderItemInput]
        $shipping: OrderShippingInput
        $billingAddress: OrderBillingAddressInput
        $shippingAddress: OrderShippingAddressInput
    ) {
        orderUpdate(
            id: $id
            orderStatusId: $orderStatusId
            taxValue: $taxValue
            discount: $discount
            discountType: $discountType
            customerId: $customerId
            customerGroupId: $customerGroupId
            items: $items
            shipping: $shipping
            billingAddress: $billingAddress
            shippingAddress: $shippingAddress
        ) {
            id
            code
            orderStatusId
            status {
                id
                color
                name
            }
            shipping {
                id
                orderId
                price
                name
                methodId
                methodType
            }
            billingAddress {
                id
                orderId
                country
                company
                city
                address
                address2
                phone
                email
                posCode
                name
                paymentMethod
            }
            shippingAddress {
                id
                orderId
                country
                company
                city
                address
                address2
                phone
                posCode
                name
                note
            }
            items {
                id
                orderId
                productBranchId
                sku
                code
                name
                image
                price
                quantity
                total
                discount
                discountType
                tax
                reward
                type
            }
            totalOrigin
            total
            discount
            discountType
            taxValue
            customerId
            user: customer {
                id
                name
                email
                phone
                avatar
            }
            customerGroupId
            ip
            forwardedIp
            userAgent
            createdAt
            updatedAt
        }
    }
`;