import gql from 'graphql-tag';

export const LOGIN = gql`
    mutation auth($email: String!, $password: String!) {
        auth(email: $email, password: $password) {
            id
            token
            phone
            email
            name
            avatar
        }
    }
`;