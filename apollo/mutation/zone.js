import gql from 'graphql-tag';

export const CREATE_ZONE = gql`
    mutation shippingZoneCreate(
        $name: String
        $country: [String]
        $postcode: [String]
    ) {
        data: shippingZoneCreate(name: $name, country: $country, postcode: $postcode) {
            id
            name
            zoneOrder
            location {
                id
                zoneId
                code
                type
            }
            methods {
                id
                zoneId
                methodType
                methodOrder
                title
                requires
                value
                taxStatus
                cost
                isEnabled
            }
        }
    }
`;

export const UPDATE_ZONE = gql`
    mutation shippingZoneUpdate(
        $id: ID_CRYPTO
        $name: String
        $country: [String]
        $postcode: [String]
    ) {
        data: shippingZoneUpdate(
            id: $id
            name: $name
            country: $country
            postcode: $postcode
        ) {
            id
            name
            zoneOrder
            location {
                id
                zoneId
                code
                type
            }
            methods {
                id
                zoneId
                methodType
                methodOrder
                title
                requires
                value
                taxStatus
                cost
                isEnabled
            }
        }
    }
`;

export const CREATE_METHOD = gql`
    mutation shippingMethodCreate(
        $zoneId: ID_CRYPTO
        $methodType: String
        $methodOrder: Float
        $title: String
        $requires: String
        $value: String
        $taxStatus: String
        $cost: Float
        $isEnabled: Boolean
    ) {
        data: shippingMethodCreate(
            zoneId: $zoneId
            methodType: $methodType
            methodOrder: $methodOrder
            title: $title
            requires: $requires
            value: $value
            taxStatus: $taxStatus
            cost: $cost
            isEnabled: $isEnabled
        ) {
            id
            zoneId
            methodType
            methodOrder
            title
            requires
            value
            taxStatus
            cost
            isEnabled
        }
    }
`;

export const UPDATE_METHOD = gql`
    mutation shippingMethodUpdate(
        $id: ID_CRYPTO
        $zoneId: ID_CRYPTO
        $methodType: String
        $title: String
        $requires: String
        $value: String
        $taxStatus: String
        $cost: String
        $isEnabled: Boolean
    ) {
        shippingMethodUpdate(
            id: $id
            zoneId: $zoneId
            methodType: $methodType
            title: $title
            requires: $requires
            value: $value
            taxStatus: $taxStatus
            cost: $cost
            isEnabled: $isEnabled
        ) {
            id
            zoneId
            methodType
            methodOrder
            title
            requires
            value
            taxStatus
            cost
            isEnabled
        }
    }
`;

export const DELETE_ZONE = gql`
    mutation shippingZoneDelete($id: [ID_CRYPTO]) {
        shippingZoneDelete(id: $id) {
            status
            message
            data
        }
    }
`;

export const DELETE_METHOD = gql`
    mutation shippingMethodDelete($id: [ID_CRYPTO]) {
        shippingMethodDelete(id: $id) {
            status
            message
            data
        }
    }
`;