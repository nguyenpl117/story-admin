import gql from 'graphql-tag';

export const UPDATE_META = gql`
mutation optionThemeUpdate(
  $metaHeadHTML: String
  $metaFooterHTML: String
  $themeNumberPhone: String
  $themeFacebookMessage: String
  $themeZaloMessage: String
  $themeSocials: String
  $googleMap: String
) {
  optionThemeUpdate(
    metaHeadHTML: $metaHeadHTML
    metaFooterHTML: $metaFooterHTML
    themeNumberPhone: $themeNumberPhone
    themeFacebookMessage: $themeFacebookMessage
    themeZaloMessage: $themeZaloMessage
    themeSocials: $themeSocials
    googleMap: $googleMap
  ) {
    metaHeadHTML
    metaHeadJSON
    metaFooterHTML
    metaFooterJSON
    themeNumberPhone
    themeFacebookMessage
    themeZaloMessage
    themeSocials
    googleMap
  }
}
`;

export const OPTION_GENERAL_UPDATE = gql`
mutation optionGeneralUpdate(
  $siteurl: String
  $home: String
  $blogname: String
  $logo: String
  $favicon: String
  $blogdescription: String
  $usersCanRegister: Boolean
  $adminEmail: String
  $defaultRole: String
  $analyticsTrackingCode: String
  $analyticsViewId: String
  $address: String
  $addressCity: String
  $zipcode: String
  $youtubeLink: String
) {
  optionGeneralUpdate(
    siteurl: $siteurl
    home: $home
    blogname: $blogname
    logo: $logo
    favicon: $favicon
    blogdescription: $blogdescription
    usersCanRegister: $usersCanRegister
    adminEmail: $adminEmail
    defaultRole: $defaultRole
    analyticsTrackingCode: $analyticsTrackingCode
    analyticsViewId: $analyticsViewId
    address: $address
    addressCity: $addressCity
    zipcode: $zipcode
    youtubeLink: $youtubeLink
  ) {
    siteurl
    home
    blogname
    logo
    favicon
    blogdescription
    usersCanRegister
    adminEmail
    defaultRole
    analyticsTrackingCode
    analyticsViewId
    address
    zipcode
  }
}
`;

export const OPTION_DISCUSSION_UPDATE = gql`
    mutation optionDiscussionUpdate(
        $defaultCommentStatus: Boolean
        $commentOrder: String
        $commentModeration: Boolean
        $commentModerationKeys: String
        $commentBlacklistKeys: String
        $commentMaxLinks: Int
    ) {
        optionDiscussionUpdate(
            defaultCommentStatus: $defaultCommentStatus
            commentOrder: $commentOrder
            commentModeration: $commentModeration
            commentModerationKeys: $commentModerationKeys
            commentBlacklistKeys: $commentBlacklistKeys
            commentMaxLinks: $commentMaxLinks
        ) {
            defaultCommentStatus
            commentOrder
            commentModeration
            commentModerationKeys
            commentBlacklistKeys
            commentMaxLinks
        }
    }
`;

export const OPTION_SOCIAL_UPDATE = gql`
    mutation optionSocialUpdate(
        $socialLogin: Boolean
        $facebookLoginEnable: Boolean
        $facebookClientId: String
        $facebookClientSecret: String
        $facebookScopes: String
        $facebookRedirectURL: String
        $googleLoginEnable: Boolean
        $googleClientId: String
        $googleClientSecret: String
        $googleScopes: String
        $googleRedirectURL: String
        $githubLoginEnable: Boolean
        $githubClientId: String
        $githubClientSecret: String
        $githubScopes: String
        $githubRedirectURL: String
        $autoRegister: Boolean
        $autoRegisterDisabledMessage: String
        $socialRegisterRole: String
    ) {
        optionSocialUpdate(
            socialLogin: $socialLogin
            facebookLoginEnable: $facebookLoginEnable
            facebookClientId: $facebookClientId
            facebookClientSecret: $facebookClientSecret
            facebookScopes: $facebookScopes
            facebookRedirectURL: $facebookRedirectURL
            googleLoginEnable: $googleLoginEnable
            googleClientId: $googleClientId
            googleClientSecret: $googleClientSecret
            googleScopes: $googleScopes
            googleRedirectURL: $googleRedirectURL
            githubLoginEnable: $githubLoginEnable
            githubClientId: $githubClientId
            githubClientSecret: $githubClientSecret
            githubScopes: $githubScopes
            githubRedirectURL: $githubRedirectURL
            autoRegister: $autoRegister
            autoRegisterDisabledMessage: $autoRegisterDisabledMessage
            socialRegisterRole: $socialRegisterRole
        ) {
            socialLogin
            facebookLoginEnable
            facebookClientId
            facebookClientSecret
            facebookScopes
            facebookRedirectURL
            googleLoginEnable
            googleClientId
            googleClientSecret
            googleScopes
            googleRedirectURL
            githubLoginEnable
            githubClientId
            githubClientSecret
            githubScopes
            githubRedirectURL
            autoRegister
            autoRegisterDisabledMessage
            socialRegisterRole
        }
    }
`;
