import gql from 'graphql-tag';

export const CREATE_BACKUP = gql`
    mutation backupCreate($name: String, $description: String) {
        backupCreate(name: $name, description: $description) {
            id
            name
            description
            size
            createdAt
            updatedAt
        }
    }
`;

export const DELETE_BACKUP = gql`
    mutation backupDelete($id: [ID_CRYPTO]) {
        backupDelete(id: $id) {
            status
            message
            data
        }
    }
`;
export const DELETE_RESTORE = gql`
    mutation backupRestore($id: ID_CRYPTO) {
        backupRestore(id: $id) {
            status
            message
        }
    }
`;
export const DOWNLOAD_BACKUP = gql`
    mutation backupDownload($id: ID_CRYPTO) {
        data: backupDownload(id: $id) {
            message
            url
        }
    }
`;
