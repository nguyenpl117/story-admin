'use strict';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 7/31/2019
 * Time: 10:38 AM
 */
export const FILTER_PAGE = function(t) {
    return [
        {
            label: t`Created at`,
            value: `createdAt`,
            type: 'datetime',
            listConditions: [
                {
                    name: t`after`,
                    value: `gt`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`before`,
                    value: `lt`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`on`,
                    value: `on`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        }
    ];
}