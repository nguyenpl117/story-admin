'use strict';

import { FETCH_TAGS } from '../apollo/query/tag';
import { FETCH_PRODUCT_VENDOR } from '../apollo/query/product-vendor';
import { FETCH_PRODUCT_TYPES } from '../apollo/query/product-type';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 7/31/2019
 * Time: 10:38 AM
 */
export const FILTER_PRODUCT = function(t) {
    return [
        {
            label: t`tagged with`,
            value: `tagId`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Query`,
                        query: FETCH_TAGS
                    }
                },
                {
                    name: t('isn`t'),
                    value: `ne`,
                    view: {
                        type: `Query`,
                        query: FETCH_TAGS
                    }
                }
            ]
        },
        {
            label: t`Product category`,
            value: `categoryId`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Query`,
                        query: FETCH_PRODUCT_TYPES
                    }
                },
                {
                    name: t(`isn\`t`),
                    value: `ne`,
                    view: {
                        type: `Query`,
                        query: FETCH_PRODUCT_TYPES
                    }
                }
            ]
        },
        {
            label: t`Product vendor`,
            value: `productVendorId`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Query`,
                        query: FETCH_PRODUCT_VENDOR
                    }
                },
                {
                    name: t(`isn\`t`),
                    value: `ne`,
                    view: {
                        type: `Query`,
                        query: FETCH_PRODUCT_VENDOR
                    }
                }
            ]
        },
        {
            label: t`Featured`,
            value: `isFeatured`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Array`,
                        data: [
                            {
                                id: '1',
                                value: true,
                                text: t`is featured`
                            },
                            {
                                id: '2',
                                value: false,
                                text: t`not featured`
                            }
                        ]
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        },
        {
            label: t`Created at`,
            value: `createdAt`,
            type: 'datetime',
            listConditions: [
                {
                    name: t`after`,
                    value: `gt`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`before`,
                    value: `lt`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`on`,
                    value: `on`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        }
    ];
}