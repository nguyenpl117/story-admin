'use strict';

import { FETCH_TAGS } from '../apollo/query/tag';
import { FETCH_CATEGORIES_POST } from '../apollo/query/category';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 7/31/2019
 * Time: 10:38 AM
 */
export const FILTER_POST = function(t) {
    return [
        {
            label: t`Tag${ 1 }`,
            value: `tagId`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Query`,
                        query: FETCH_TAGS
                    }
                },
                {
                    name: t('isn`t'),
                    value: `ne`,
                    view: {
                        type: `Query`,
                        query: FETCH_TAGS
                    }
                }
            ]
        },
        {
            label: t`Category`,
            value: `categoryId`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Query`,
                        query: FETCH_CATEGORIES_POST
                    }
                },
                {
                    name: t(`isn\`t`),
                    value: `ne`,
                    view: {
                        type: `Query`,
                        query: FETCH_CATEGORIES_POST
                    }
                }
            ]
        },
        {
            label: t`Featured`,
            value: `isFeatured`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Array`,
                        data: [
                            {
                                id: '1',
                                value: true,
                                text: t`is featured`
                            },
                            {
                                id: '2',
                                value: false,
                                text: t`not featured`
                            }
                        ]
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        },
        {
            label: t`Created at`,
            value: `createdAt`,
            type: 'datetime',
            listConditions: [
                {
                    name: t`after`,
                    value: `gt`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`before`,
                    value: `lt`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`on`,
                    value: `on`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        }
    ];
}