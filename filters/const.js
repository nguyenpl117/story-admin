'use strict';
/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 8/1/2019
 * Time: 9:08 PM
 */
export const FilterType = {
    ARRAY: 'Array',
    QUERY: 'Query',
    TEXT: 'Text',
    NUMBER: 'Number',
    DATE: 'Date'
}