'use strict';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 7/31/2019
 * Time: 10:38 AM
 */
export const FILTER_CONTACT = function(t) {
    return [
        {
            label: t`Name`,
            value: `name`,
            listConditions: [
                {
                    name: t`Contains`,
                    value: `like`,
                    view: {
                        type: `Text`
                    }
                },
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Text`
                    }
                },
                {
                    name: t('isn`t'),
                    value: `ne`,
                    view: {
                        type: `Text`
                    }
                }
            ]
        },
        {
            label: t`Email`,
            value: `email`,
            listConditions: [
                {
                    name: t`Contains`,
                    value: `like`,
                    view: {
                        type: `Text`
                    }
                },
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Text`
                    }
                },
                {
                    name: t('isn`t'),
                    value: `ne`,
                    view: {
                        type: `Text`
                    }
                }
            ]
        },
        {
            label: t`Phone`,
            value: `phone`,
            listConditions: [
                {
                    name: t`Contains`,
                    value: `like`,
                    view: {
                        type: `Text`
                    }
                },
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Text`
                    }
                },
                {
                    name: t('isn`t'),
                    value: `ne`,
                    view: {
                        type: `Text`
                    }
                }
            ]
        },
        {
            label: t`Status`,
            value: `status`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Array`,
                        data: [
                            {
                                id: 'read',
                                value: 'read',
                                text: 'Read'
                            },
                            {
                                id: 'unRead',
                                value: 'unRead',
                                text: 'UnRead'
                            }
                        ]
                    }
                },
                {
                    name: t('isn`t'),
                    value: `ne`,
                    view: {
                        type: `Array`,
                        data: [
                            {
                                id: 'read',
                                value: 'read',
                                text: 'Read'
                            },
                            {
                                id: 'unRead',
                                value: 'unRead',
                                text: 'UnRead'
                            }
                        ]
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        },
        {
            label: t`Created at`,
            value: `createdAt`,
            type: 'datetime',
            listConditions: [
                {
                    name: t`after`,
                    value: `gt`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`before`,
                    value: `lt`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`on`,
                    value: `on`,
                    type: 'datetime',
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        }
    ];
}