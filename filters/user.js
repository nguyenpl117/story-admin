'use strict';

import { FilterType } from './const';
import { FETCH_ROLES } from '../apollo/query/role';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 7/31/2019
 * Time: 10:38 AM
 */
export const FILTER_USER = function(t) {
    return [
        {
            label: t`Gender`,
            value: `gender`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: `Array`,
                        data: [
                            {
                                id: '1',
                                value: '1',
                                text: t`male`
                            },
                            {
                                id: '2',
                                value: '2',
                                text: t`female`
                            }
                        ]
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        },
        {
            label: t`Role`,
            value: `roleId`,
            listConditions: [
                {
                    name: t`is`,
                    value: `eq`,
                    view: {
                        type: FilterType.QUERY,
                        query: FETCH_ROLES
                    }
                },
                {
                    name: t(`isn\`t`),
                    value: `eq`,
                    view: {
                        type: FilterType.QUERY,
                        query: FETCH_ROLES
                    }
                },

            ]
        },
        {
            label: t`Birthday date`,
            value: `dob`,
            listConditions: [
                {
                    name: t`after`,
                    value: `gt`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`before`,
                    value: `lt`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`on`,
                    value: `on`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        },
        {
            label: t`Created at`,
            value: `createdAt`,
            type: 'datetime',
            listConditions: [
                {
                    name: t`after`,
                    value: `gt`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`before`,
                    value: `lt`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`on`,
                    value: `on`,
                    view: {
                        type: `Date`
                    }
                },
                {
                    name: t`has any value`,
                    value: `ne`
                },
                {
                    name: t`is unknown`,
                    value: `eq`
                }
            ]
        }
    ];
}