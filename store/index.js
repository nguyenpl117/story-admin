import { PROFILE } from '../apollo/query/user';
import { OPTION_GENERAL } from '../apollo/query/options';

export const state = () => ({
    locales: ['vi'],
    locale: 'vi',
    drawer: null,
    rightDrawer: null,
    miniVariant: false,
    clipped: true,
    auth: {},
    formToolbarActive: false,
    optionGeneral: {
        siteurl: 'http://localhost:3000'
    }
});

export const mutations = {
    SET_LANG(state, locale) {
        if ( state.locales.indexOf(locale) !== -1 ) {
            state.locale = locale
        }
    },
    SET_DRAWER(state, value) {
        state.drawer = value;
    },
    SET_MINI_VARIANT(state, value) {
        state.miniVariant = value;
    },
    SET_RIGHT_DRAWER(state, value) {
        state.rightDrawer = value;
    },
    SET_CLIPPED(state, value) {
        state.clipped = value;
    },
    setAuth(state, value) {
        state.auth = value;
    },
    setActiveFormToolbar(state, val) {
        state.formToolbarActive = val;
    }
};

export const actions = {
    async nuxtServerInit({ commit }, { app, req, env }) {
        const client = app.apolloProvider.clients.defaultClient;

        const settingsGeneral = await client.query({
            query: OPTION_GENERAL
        }).then((data) => {
            if ( data && data.data ) {
                return data.data;
            }
        }).catch(({ graphQLErrors }) => {
            if ( env.NODE_ENV !== 'production' ) {
                console.log('Unauthenticated');
            }
        });

        if ( settingsGeneral && settingsGeneral.data ) {
            app.store.state.optionGeneral = settingsGeneral.data;
        }

        const res = await client.query({
            query: PROFILE,
        }).catch(({ graphQLErrors }) => {
            if ( env.NODE_ENV !== 'production' ) {
                console.log('Unauthenticated');
            }
        });

        if ( res ) {
            const { data } = res;

            if ( data.profile ) {
                commit('setAuth', data.profile || {});
            }
        }
    }
};
