'use strict';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 9/9/2019
 * Time: 3:39 PM
 */
export const state = () => ({
    master: {}
});

export const mutations = {
    master(state, master) {
        state.master = master || {};
    }
};