export default function({ store, redirect }) {
    // If the _user is not authenticated
    if ( ! store.state.auth || ! store.state.auth.id ) {
        return redirect('/login')
    }
}
