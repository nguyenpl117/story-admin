import Sketch from 'vue-color/src/components/Sketch';
import Vue from 'vue';

Vue.component('sketch-picker', Sketch);