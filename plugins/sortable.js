import Vue from 'vue';
// import Sortable from 'vue-sortable';

const vSortable = {}
const Sortable = typeof require === 'function'
    ? require('sortablejs')
    : window.Sortable

vSortable.config = {}

vSortable.install = function(Vue) {
    Vue.directive('sortable', {
        inserted: function(el, binding) {
            Sortable.Sortable.create(el, binding.value || {});
        }
    });
}

if ( typeof window !== 'undefined' && window.Vue ) {
    window.vSortable = vSortable
}

Vue.use(vSortable)