import Vue from 'vue';
import { createFetch, deleteFetch, updateFetch } from '../components/utils/utils';

Vue.mixin({
    data() {
        return {
            validateOnFocusForm(formName = 'form') {
                let form = this.$refs[formName];

                // form.resetValidation();

                this.errors = {};

                form.inputs.some(item => {
                    if ( item.hasError ) {
                        item.$emit('update:error', true);
                        item.focus && item.focus();
                        return true;
                    }
                });

                return form.validate();
            },
            serviceHttp: {
                createFetch: ({ mutation, variables, refetchQueries = [], options = {} }, callback) => {
                    return createFetch.bind(this)({ mutation, variables, refetchQueries, options }, callback);
                },
                updateFetch: (mutation, variables, options = {}, callback) => {
                    if ( typeof options === 'function' ) {
                        callback = options;
                    }
                    return updateFetch.call(this, { mutation, variables, refetchQueries: [] }, callback);
                },
                deleteFetch: (mutation, id, refetchQueries) => {
                    return deleteFetch.call(this, mutation, id, refetchQueries);
                },
                updateRefetch: ({ mutation, variables, refetchQueries = [], options = {} }, callback) => {
                    return updateFetch.call(this, { mutation, variables, refetchQueries }, callback);
                }
            }
        }
    }
})