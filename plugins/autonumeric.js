import Vue from 'vue'
import AutoNumeric from 'autonumeric';
import { convertStringToNumber } from '../components/utils/utils';
import _ from 'lodash';

let options = {
    // minimumValue: -1000000000,
    // maximumValue: 1000000000,
    decimalPlaces: 2,
    allowDecimalPadding: false,
    showWarnings: false,
    emptyInputBehavior: "press",
    currencySymbol: '₫',
    currencySymbolPlacement: 's', // s Phải, p trái
    styleRules: {
        'ranges': [
            { 'min': 0, 'max': 0.01, 'class': 'error--text' }
        ]
    }
}

Vue.directive('price-number', {
    bind: function(el, binding, vnode) {
        let v = vnode.context.options || {};
        let opt = { ...options, ...v };

        let input = el.querySelector('input');
        input.setAttribute('type', 'text');
        el.anElement = new AutoNumeric(input, opt);
    },
    update: function(el, binding, vnode) {
        let v = vnode.context.options || {};
        let opt = { ...options, ...v };
        let input = el.querySelector('input');

        if ( ! vnode.data.model.value ) {
            el.anElement && el.anElement.clear(false);
            return;
        }

        if ( typeof vnode.data.model.value === 'number' ) {
            _.set(vnode.context, binding.expression, AutoNumeric.format(vnode.data.model.value, opt));
        }

        if ( ! input.getAttribute('autonumeric') ) {
            el.anElement.remove();
            el.anElement = new AutoNumeric(input, convertStringToNumber(vnode.data.model.value), opt);
            _.set(vnode.context, binding.expression, AutoNumeric.format(convertStringToNumber(vnode.data.model.value), opt));
        }

        input.setAttribute('autonumeric', 'true');
    },
});

Vue.filter('toCurrentcy', function(value, currencySymbol = options.currencySymbol) {
    if ( typeof value !== 'number' ) {
        return value;
    }

    let opt = { ...options, currencySymbol };

    return AutoNumeric.format(convertStringToNumber(value), opt);
});