import Vue from 'vue'
import Editor from '@tinymce/tinymce-vue';

Vue.component('tiny-mce', Editor);
