import Vue from 'vue'
import CKEditor from '@ckeditor/ckeditor5-vue';
import componentCKEditor from '../components/helper/CKEditor';

Vue.use(CKEditor);
Vue.component('CKEditor', componentCKEditor);