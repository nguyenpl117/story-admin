import Vue from 'vue'
import VueCodemirror from 'vue-codemirror';
// require styles
import 'codemirror/lib/codemirror.css'
// language
import 'codemirror/mode/css/css.js'

import 'codemirror/theme/paraiso-light.css'
// require active-line.js
import 'codemirror/addon/selection/active-line.js'
import 'codemirror/addon/hint/show-hint.css'
import 'codemirror/addon/hint/show-hint.js'
import 'codemirror/addon/hint/css-hint.js'

// require more codemirror resource...

// you can set default global options and events when use
Vue.use(VueCodemirror, /* {
  options: { theme: 'base16-dark', ... },
  events: ['scroll', ...]
} */)
