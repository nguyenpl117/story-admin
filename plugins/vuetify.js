import Vue from 'vue'

import Vuetify from 'vuetify';
// index.js or main.js
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
    'theme': {
        'primary': '#1976d2',
        'accent': '#424242',
        'secondary': '#ff8f00',
        'info': '#26a69a',
        'warning': '#ffc107',
        'error': '#dd2c00',
        'success': '#00e676'
    }
}).$mount('#app')
