require('dotenv').config();
const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = ! (process.env.NODE_ENV === 'production')

async function start() {
    // Init Nuxt.js
    const nuxt = new Nuxt(config)

    const { host, port } = nuxt.options.server

    // Build only in dev mode
    if ( config.dev ) {
        const builder = new Builder(nuxt)
        await builder.build()
    } else {
        await nuxt.ready()
    }

    app.get('/laraberg/blocks', function(req, res) {
        res.json([
            {
                'id': 3,
                'raw_title': 'Tewedgh',
                'raw_content': '<!-- wp:paragraph -->\n<p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia.<\/p>\n<!-- \/wp:paragraph -->',
                'rendered_content': '<!-- wp:paragraph -->\n<p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia.<\/p>\n<!-- \/wp:paragraph -->',
                'status': 'publish',
                'slug': 'tewedgh',
                'type': 'wp_block',
                'created_at': '2019-05-21 11:09:17',
                'updated_at': '2019-05-24 16:10:41',
                'content': {
                    'raw': '<!-- wp:paragraph -->\n<p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia.<\/p>\n<!-- \/wp:paragraph -->',
                    'rendered': '<!-- wp:paragraph -->\n<p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia.<\/p>\n<!-- \/wp:paragraph -->'
                },
                'title': { 'raw': 'Tewedgh' }
            },
            {
                'id': 4,
                'raw_title': 'pref',
                'raw_content': '<!-- wp:paragraph {"backgroundColor":"vivid-red","fontSize":"medium"} -->\n<p class="has-background has-medium-font-size has-vivid-red-background-color">Id viverra habitasse posuere taciti aenean facilisi, luctus natoque porttitor non quis nisl maecenas, vitae leo to<strong>rtor vehicula pharetra. P<\/strong>haretra aliquet ornare vel eu ac sem elementum consectetur in, malesuada per nisi tellus interdum natoque hendrerit felis, habitasse pretium fringilla rutrum praesent aliquam ex nostra.<\/p>\n<!-- \/wp:paragraph -->',
                'rendered_content': '<!-- wp:paragraph {"backgroundColor":"vivid-red","fontSize":"medium"} -->\n<p class="has-background has-medium-font-size has-vivid-red-background-color">Id viverra habitasse posuere taciti aenean facilisi, luctus natoque porttitor non quis nisl maecenas, vitae leo to<strong>rtor vehicula pharetra. P<\/strong>haretra aliquet ornare vel eu ac sem elementum consectetur in, malesuada per nisi tellus interdum natoque hendrerit felis, habitasse pretium fringilla rutrum praesent aliquam ex nostra.<\/p>\n<!-- \/wp:paragraph -->',
                'status': 'publish',
                'slug': 'pref',
                'type': 'wp_block',
                'created_at': '2019-05-21 12:25:03',
                'updated_at': '2019-05-29 10:50:25',
                'content': {
                    'raw': '<!-- wp:paragraph {"backgroundColor":"vivid-red","fontSize":"medium"} -->\n<p class="has-background has-medium-font-size has-vivid-red-background-color">Id viverra habitasse posuere taciti aenean facilisi, luctus natoque porttitor non quis nisl maecenas, vitae leo to<strong>rtor vehicula pharetra. P<\/strong>haretra aliquet ornare vel eu ac sem elementum consectetur in, malesuada per nisi tellus interdum natoque hendrerit felis, habitasse pretium fringilla rutrum praesent aliquam ex nostra.<\/p>\n<!-- \/wp:paragraph -->',
                    'rendered': '<!-- wp:paragraph {"backgroundColor":"vivid-red","fontSize":"medium"} -->\n<p class="has-background has-medium-font-size has-vivid-red-background-color">Id viverra habitasse posuere taciti aenean facilisi, luctus natoque porttitor non quis nisl maecenas, vitae leo to<strong>rtor vehicula pharetra. P<\/strong>haretra aliquet ornare vel eu ac sem elementum consectetur in, malesuada per nisi tellus interdum natoque hendrerit felis, habitasse pretium fringilla rutrum praesent aliquam ex nostra.<\/p>\n<!-- \/wp:paragraph -->'
                },
                'title': { 'raw': 'pref' }
            },
            {
                'id': 8,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:gallery {"ids":[null]} -->\n<ul class="wp-block-gallery columns-1 is-cropped"><li class="blocks-gallery-item"><figure><img src="http:\/\/demo.laraberg.io\/photos\/shares\/andalan.png"\/><\/figure><\/li><\/ul>\n<!-- \/wp:gallery -->',
                'rendered_content': '<!-- wp:gallery {"ids":[null]} -->\n<ul class="wp-block-gallery columns-1 is-cropped"><li class="blocks-gallery-item"><figure><img src="http:\/\/demo.laraberg.io\/photos\/shares\/andalan.png"\/><\/figure><\/li><\/ul>\n<!-- \/wp:gallery -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-05-23 09:39:40',
                'updated_at': '2019-05-23 09:39:40',
                'content': {
                    'raw': '<!-- wp:gallery {"ids":[null]} -->\n<ul class="wp-block-gallery columns-1 is-cropped"><li class="blocks-gallery-item"><figure><img src="http:\/\/demo.laraberg.io\/photos\/shares\/andalan.png"\/><\/figure><\/li><\/ul>\n<!-- \/wp:gallery -->',
                    'rendered': '<!-- wp:gallery {"ids":[null]} -->\n<ul class="wp-block-gallery columns-1 is-cropped"><li class="blocks-gallery-item"><figure><img src="http:\/\/demo.laraberg.io\/photos\/shares\/andalan.png"\/><\/figure><\/li><\/ul>\n<!-- \/wp:gallery -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            },
            {
                'id': 9,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:paragraph -->\n<p>Test<\/p>\n<!-- \/wp:paragraph -->',
                'rendered_content': '<!-- wp:paragraph -->\n<p>Test<\/p>\n<!-- \/wp:paragraph -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-05-24 11:09:10',
                'updated_at': '2019-05-24 11:09:10',
                'content': {
                    'raw': '<!-- wp:paragraph -->\n<p>Test<\/p>\n<!-- \/wp:paragraph -->',
                    'rendered': '<!-- wp:paragraph -->\n<p>Test<\/p>\n<!-- \/wp:paragraph -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            },
            {
                'id': 10,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:video {"align":"center"} -->\n<figure class="wp-block-video aligncenter"><video controls src="http:\/\/demo.laraberg.io\/files\/shares\/mov_bbb.mp4"><\/video><\/figure>\n<!-- \/wp:video -->',
                'rendered_content': '<!-- wp:video {"align":"center"} -->\n<figure class="wp-block-video aligncenter"><video controls src="http:\/\/demo.laraberg.io\/files\/shares\/mov_bbb.mp4"><\/video><\/figure>\n<!-- \/wp:video -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-06-02 08:28:38',
                'updated_at': '2019-06-02 08:28:38',
                'content': {
                    'raw': '<!-- wp:video {"align":"center"} -->\n<figure class="wp-block-video aligncenter"><video controls src="http:\/\/demo.laraberg.io\/files\/shares\/mov_bbb.mp4"><\/video><\/figure>\n<!-- \/wp:video -->',
                    'rendered': '<!-- wp:video {"align":"center"} -->\n<figure class="wp-block-video aligncenter"><video controls src="http:\/\/demo.laraberg.io\/files\/shares\/mov_bbb.mp4"><\/video><\/figure>\n<!-- \/wp:video -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            },
            {
                'id': 11,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:button {"backgroundColor":"vivid-red","textColor":"pale-pink","align":"center","className":"is-style-default"} -->\n<div class="wp-block-button aligncenter is-style-default"><a class="wp-block-button__link has-text-color has-pale-pink-color has-background has-vivid-red-background-color" href="asdasdas"><\/a><\/div>\n<!-- \/wp:button -->',
                'rendered_content': '<!-- wp:button {"backgroundColor":"vivid-red","textColor":"pale-pink","align":"center","className":"is-style-default"} -->\n<div class="wp-block-button aligncenter is-style-default"><a class="wp-block-button__link has-text-color has-pale-pink-color has-background has-vivid-red-background-color" href="asdasdas"><\/a><\/div>\n<!-- \/wp:button -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-06-04 07:25:41',
                'updated_at': '2019-06-04 07:25:41',
                'content': {
                    'raw': '<!-- wp:button {"backgroundColor":"vivid-red","textColor":"pale-pink","align":"center","className":"is-style-default"} -->\n<div class="wp-block-button aligncenter is-style-default"><a class="wp-block-button__link has-text-color has-pale-pink-color has-background has-vivid-red-background-color" href="asdasdas"><\/a><\/div>\n<!-- \/wp:button -->',
                    'rendered': '<!-- wp:button {"backgroundColor":"vivid-red","textColor":"pale-pink","align":"center","className":"is-style-default"} -->\n<div class="wp-block-button aligncenter is-style-default"><a class="wp-block-button__link has-text-color has-pale-pink-color has-background has-vivid-red-background-color" href="asdasdas"><\/a><\/div>\n<!-- \/wp:button -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            },
            {
                'id': 12,
                'raw_title': 'Testdsfsdf',
                'raw_content': '<!-- wp:button {"align":"center","className":"is-style-outline"} -->\n<div class="wp-block-button aligncenter is-style-outline"><a class="wp-block-button__link" href="fgddfgdfgdf"><\/a><\/div>\n<!-- \/wp:button -->',
                'rendered_content': '<!-- wp:button {"align":"center","className":"is-style-outline"} -->\n<div class="wp-block-button aligncenter is-style-outline"><a class="wp-block-button__link" href="fgddfgdfgdf"><\/a><\/div>\n<!-- \/wp:button -->',
                'status': 'publish',
                'slug': 'testdsfsdf',
                'type': 'wp_block',
                'created_at': '2019-06-12 15:13:20',
                'updated_at': '2019-06-22 01:02:41',
                'content': {
                    'raw': '<!-- wp:button {"align":"center","className":"is-style-outline"} -->\n<div class="wp-block-button aligncenter is-style-outline"><a class="wp-block-button__link" href="fgddfgdfgdf"><\/a><\/div>\n<!-- \/wp:button -->',
                    'rendered': '<!-- wp:button {"align":"center","className":"is-style-outline"} -->\n<div class="wp-block-button aligncenter is-style-outline"><a class="wp-block-button__link" href="fgddfgdfgdf"><\/a><\/div>\n<!-- \/wp:button -->'
                },
                'title': { 'raw': 'Testdsfsdf' }
            },
            {
                'id': 13,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:list {"ordered":true} -->\n<ol><li>Th\u1eddi gian b\u1eaft \u0111\u1ea7u: Ngay khi k\u00fd h\u1ee3p \u0111\u1ed3ng.<\/li><\/ol>\n<!-- \/wp:list -->\n\n<!-- wp:list {"ordered":true} -->\n<ol><li>Th\u1eddi gian cung c\u1ea5p h\u1ed3 s\u01a1 th\u1ef1c hi\u1ec7n h\u1ee3p \u0111\u1ed3ng: \u0110\u1ec1 ngh\u1ecb Qu\u00fd C\u00f4ng ty cung c\u1ea5p \u0111\u1ea7y \u0111\u1ee7 th\u00f4ng tin, s\u1ed1 li\u1ec7u \u0111\u1ec3 th\u1ef1c hi\u1ec7n h\u1ee3p \u0111\u1ed3ng (\u0110\u00ednh k\u00e8m ph\u1ee5 l\u1ee5c cung c\u1ea5p th\u00f4ng tin) trong v\u00f2ng 10 ng\u00e0y k\u1ec3 t\u1eeb ng\u00e0y h\u1ee3p \u0111\u1ed3ng \u0111\u01b0\u1ee3c k\u00fd k\u1ebft.<\/li><\/ol>\n<!-- \/wp:list -->\n\n<!-- wp:list -->\n<ul><li>Th\u1eddi gian th\u1ef1c hi\u1ec7n H\u1ee3p \u0111\u1ed3ng: Trong v\u00f2ng 65-86 ng\u00e0y l\u00e0m vi\u1ec7c, k\u1ec3 t\u1eeb khi b\u00ean A cung c\u1ea5p \u0111\u1ea7y \u0111\u1ee7 c\u00e1c ch\u1ee9ng t\u1eeb, h\u1ed3 s\u01a1 \u0111\u00e1p \u1ee9ng cho vi\u1ec7c l\u1eadp \u0110TM v\u00e0 thanh to\u00e1n \u0111\u1ee3t 01 cho b\u00ean B.<\/li><\/ul>\n<!-- \/wp:list -->',
                'rendered_content': '<!-- wp:list {"ordered":true} -->\n<ol><li>Th\u1eddi gian b\u1eaft \u0111\u1ea7u: Ngay khi k\u00fd h\u1ee3p \u0111\u1ed3ng.<\/li><\/ol>\n<!-- \/wp:list -->\n\n<!-- wp:list {"ordered":true} -->\n<ol><li>Th\u1eddi gian cung c\u1ea5p h\u1ed3 s\u01a1 th\u1ef1c hi\u1ec7n h\u1ee3p \u0111\u1ed3ng: \u0110\u1ec1 ngh\u1ecb Qu\u00fd C\u00f4ng ty cung c\u1ea5p \u0111\u1ea7y \u0111\u1ee7 th\u00f4ng tin, s\u1ed1 li\u1ec7u \u0111\u1ec3 th\u1ef1c hi\u1ec7n h\u1ee3p \u0111\u1ed3ng (\u0110\u00ednh k\u00e8m ph\u1ee5 l\u1ee5c cung c\u1ea5p th\u00f4ng tin) trong v\u00f2ng 10 ng\u00e0y k\u1ec3 t\u1eeb ng\u00e0y h\u1ee3p \u0111\u1ed3ng \u0111\u01b0\u1ee3c k\u00fd k\u1ebft.<\/li><\/ol>\n<!-- \/wp:list -->\n\n<!-- wp:list -->\n<ul><li>Th\u1eddi gian th\u1ef1c hi\u1ec7n H\u1ee3p \u0111\u1ed3ng: Trong v\u00f2ng 65-86 ng\u00e0y l\u00e0m vi\u1ec7c, k\u1ec3 t\u1eeb khi b\u00ean A cung c\u1ea5p \u0111\u1ea7y \u0111\u1ee7 c\u00e1c ch\u1ee9ng t\u1eeb, h\u1ed3 s\u01a1 \u0111\u00e1p \u1ee9ng cho vi\u1ec7c l\u1eadp \u0110TM v\u00e0 thanh to\u00e1n \u0111\u1ee3t 01 cho b\u00ean B.<\/li><\/ul>\n<!-- \/wp:list -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-06-19 14:42:12',
                'updated_at': '2019-06-19 14:42:12',
                'content': {
                    'raw': '<!-- wp:list {"ordered":true} -->\n<ol><li>Th\u1eddi gian b\u1eaft \u0111\u1ea7u: Ngay khi k\u00fd h\u1ee3p \u0111\u1ed3ng.<\/li><\/ol>\n<!-- \/wp:list -->\n\n<!-- wp:list {"ordered":true} -->\n<ol><li>Th\u1eddi gian cung c\u1ea5p h\u1ed3 s\u01a1 th\u1ef1c hi\u1ec7n h\u1ee3p \u0111\u1ed3ng: \u0110\u1ec1 ngh\u1ecb Qu\u00fd C\u00f4ng ty cung c\u1ea5p \u0111\u1ea7y \u0111\u1ee7 th\u00f4ng tin, s\u1ed1 li\u1ec7u \u0111\u1ec3 th\u1ef1c hi\u1ec7n h\u1ee3p \u0111\u1ed3ng (\u0110\u00ednh k\u00e8m ph\u1ee5 l\u1ee5c cung c\u1ea5p th\u00f4ng tin) trong v\u00f2ng 10 ng\u00e0y k\u1ec3 t\u1eeb ng\u00e0y h\u1ee3p \u0111\u1ed3ng \u0111\u01b0\u1ee3c k\u00fd k\u1ebft.<\/li><\/ol>\n<!-- \/wp:list -->\n\n<!-- wp:list -->\n<ul><li>Th\u1eddi gian th\u1ef1c hi\u1ec7n H\u1ee3p \u0111\u1ed3ng: Trong v\u00f2ng 65-86 ng\u00e0y l\u00e0m vi\u1ec7c, k\u1ec3 t\u1eeb khi b\u00ean A cung c\u1ea5p \u0111\u1ea7y \u0111\u1ee7 c\u00e1c ch\u1ee9ng t\u1eeb, h\u1ed3 s\u01a1 \u0111\u00e1p \u1ee9ng cho vi\u1ec7c l\u1eadp \u0110TM v\u00e0 thanh to\u00e1n \u0111\u1ee3t 01 cho b\u00ean B.<\/li><\/ul>\n<!-- \/wp:list -->',
                    'rendered': '<!-- wp:list {"ordered":true} -->\n<ol><li>Th\u1eddi gian b\u1eaft \u0111\u1ea7u: Ngay khi k\u00fd h\u1ee3p \u0111\u1ed3ng.<\/li><\/ol>\n<!-- \/wp:list -->\n\n<!-- wp:list {"ordered":true} -->\n<ol><li>Th\u1eddi gian cung c\u1ea5p h\u1ed3 s\u01a1 th\u1ef1c hi\u1ec7n h\u1ee3p \u0111\u1ed3ng: \u0110\u1ec1 ngh\u1ecb Qu\u00fd C\u00f4ng ty cung c\u1ea5p \u0111\u1ea7y \u0111\u1ee7 th\u00f4ng tin, s\u1ed1 li\u1ec7u \u0111\u1ec3 th\u1ef1c hi\u1ec7n h\u1ee3p \u0111\u1ed3ng (\u0110\u00ednh k\u00e8m ph\u1ee5 l\u1ee5c cung c\u1ea5p th\u00f4ng tin) trong v\u00f2ng 10 ng\u00e0y k\u1ec3 t\u1eeb ng\u00e0y h\u1ee3p \u0111\u1ed3ng \u0111\u01b0\u1ee3c k\u00fd k\u1ebft.<\/li><\/ol>\n<!-- \/wp:list -->\n\n<!-- wp:list -->\n<ul><li>Th\u1eddi gian th\u1ef1c hi\u1ec7n H\u1ee3p \u0111\u1ed3ng: Trong v\u00f2ng 65-86 ng\u00e0y l\u00e0m vi\u1ec7c, k\u1ec3 t\u1eeb khi b\u00ean A cung c\u1ea5p \u0111\u1ea7y \u0111\u1ee7 c\u00e1c ch\u1ee9ng t\u1eeb, h\u1ed3 s\u01a1 \u0111\u00e1p \u1ee9ng cho vi\u1ec7c l\u1eadp \u0110TM v\u00e0 thanh to\u00e1n \u0111\u1ee3t 01 cho b\u00ean B.<\/li><\/ul>\n<!-- \/wp:list -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            },
            {
                'id': 15,
                'raw_title': 'Nieuwe paragraaf',
                'raw_content': '<!-- wp:paragraph -->\n<p>Nieuwe Paragraaf<\/p>\n<!-- \/wp:paragraph -->',
                'rendered_content': '<!-- wp:paragraph -->\n<p>Nieuwe Paragraaf<\/p>\n<!-- \/wp:paragraph -->',
                'status': 'publish',
                'slug': 'nieuwe-paragraaf',
                'type': 'wp_block',
                'created_at': '2019-06-23 21:18:45',
                'updated_at': '2019-06-23 21:18:49',
                'content': {
                    'raw': '<!-- wp:paragraph -->\n<p>Nieuwe Paragraaf<\/p>\n<!-- \/wp:paragraph -->',
                    'rendered': '<!-- wp:paragraph -->\n<p>Nieuwe Paragraaf<\/p>\n<!-- \/wp:paragraph -->'
                },
                'title': { 'raw': 'Nieuwe paragraaf' }
            },
            {
                'id': 16,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:list -->\n<ul><li>Hello there!<\/li><li>How are you today<\/li><\/ul>\n<!-- \/wp:list -->',
                'rendered_content': '<!-- wp:list -->\n<ul><li>Hello there!<\/li><li>How are you today<\/li><\/ul>\n<!-- \/wp:list -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-06-25 12:34:44',
                'updated_at': '2019-06-25 12:34:44',
                'content': {
                    'raw': '<!-- wp:list -->\n<ul><li>Hello there!<\/li><li>How are you today<\/li><\/ul>\n<!-- \/wp:list -->',
                    'rendered': '<!-- wp:list -->\n<ul><li>Hello there!<\/li><li>How are you today<\/li><\/ul>\n<!-- \/wp:list -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            },
            {
                'id': 17,
                'raw_title': 'asdf',
                'raw_content': '<!-- wp:columns {"align":"full"} -->\n<div class="wp-block-columns alignfull has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p>adff1<\/p>\n<!-- \/wp:paragraph --><\/div>\n<!-- \/wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><\/div>\n<!-- \/wp:column --><\/div>\n<!-- \/wp:columns -->',
                'rendered_content': '<!-- wp:columns {"align":"full"} -->\n<div class="wp-block-columns alignfull has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p>adff1<\/p>\n<!-- \/wp:paragraph --><\/div>\n<!-- \/wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><\/div>\n<!-- \/wp:column --><\/div>\n<!-- \/wp:columns -->',
                'status': 'publish',
                'slug': 'asdf',
                'type': 'wp_block',
                'created_at': '2019-06-25 15:49:43',
                'updated_at': '2019-06-25 15:49:47',
                'content': {
                    'raw': '<!-- wp:columns {"align":"full"} -->\n<div class="wp-block-columns alignfull has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p>adff1<\/p>\n<!-- \/wp:paragraph --><\/div>\n<!-- \/wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><\/div>\n<!-- \/wp:column --><\/div>\n<!-- \/wp:columns -->',
                    'rendered': '<!-- wp:columns {"align":"full"} -->\n<div class="wp-block-columns alignfull has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p>adff1<\/p>\n<!-- \/wp:paragraph --><\/div>\n<!-- \/wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><\/div>\n<!-- \/wp:column --><\/div>\n<!-- \/wp:columns -->'
                },
                'title': { 'raw': 'asdf' }
            },
            {
                'id': 18,
                'raw_title': 'test',
                'raw_content': '<!-- wp:paragraph {"align":"left"} -->\n<p style="text-align:left">test<\/p>\n<!-- \/wp:paragraph -->',
                'rendered_content': '<!-- wp:paragraph {"align":"left"} -->\n<p style="text-align:left">test<\/p>\n<!-- \/wp:paragraph -->',
                'status': 'publish',
                'slug': 'test',
                'type': 'wp_block',
                'created_at': '2019-06-27 11:09:57',
                'updated_at': '2019-06-27 11:10:02',
                'content': {
                    'raw': '<!-- wp:paragraph {"align":"left"} -->\n<p style="text-align:left">test<\/p>\n<!-- \/wp:paragraph -->',
                    'rendered': '<!-- wp:paragraph {"align":"left"} -->\n<p style="text-align:left">test<\/p>\n<!-- \/wp:paragraph -->'
                },
                'title': { 'raw': 'test' }
            },
            {
                'id': 19,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:paragraph {"align":"right"} -->\n<p style="text-align:right">\u0622\u06cc\u0627 \u0635\u0628\u062d \u06a9\u0647 \u0627\u0632 \u062e\u0648\u0627\u0628 \u0628\u0631\u0645\u06cc\u200c\u062e\u06cc\u0632\u06cc\u062f \u0627\u062d\u0633\u0627\u0633 \u06a9\u0633\u0644\u06cc \u0648 \u0628\u06cc\u200c\u062d\u0627\u0644\u06cc \u062f\u0627\u0631\u06cc\u062f\u061f<\/p>\n<!-- \/wp:paragraph -->',
                'rendered_content': '<!-- wp:paragraph {"align":"right"} -->\n<p style="text-align:right">\u0622\u06cc\u0627 \u0635\u0628\u062d \u06a9\u0647 \u0627\u0632 \u062e\u0648\u0627\u0628 \u0628\u0631\u0645\u06cc\u200c\u062e\u06cc\u0632\u06cc\u062f \u0627\u062d\u0633\u0627\u0633 \u06a9\u0633\u0644\u06cc \u0648 \u0628\u06cc\u200c\u062d\u0627\u0644\u06cc \u062f\u0627\u0631\u06cc\u062f\u061f<\/p>\n<!-- \/wp:paragraph -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-06-27 18:50:33',
                'updated_at': '2019-06-27 18:50:33',
                'content': {
                    'raw': '<!-- wp:paragraph {"align":"right"} -->\n<p style="text-align:right">\u0622\u06cc\u0627 \u0635\u0628\u062d \u06a9\u0647 \u0627\u0632 \u062e\u0648\u0627\u0628 \u0628\u0631\u0645\u06cc\u200c\u062e\u06cc\u0632\u06cc\u062f \u0627\u062d\u0633\u0627\u0633 \u06a9\u0633\u0644\u06cc \u0648 \u0628\u06cc\u200c\u062d\u0627\u0644\u06cc \u062f\u0627\u0631\u06cc\u062f\u061f<\/p>\n<!-- \/wp:paragraph -->',
                    'rendered': '<!-- wp:paragraph {"align":"right"} -->\n<p style="text-align:right">\u0622\u06cc\u0627 \u0635\u0628\u062d \u06a9\u0647 \u0627\u0632 \u062e\u0648\u0627\u0628 \u0628\u0631\u0645\u06cc\u200c\u062e\u06cc\u0632\u06cc\u062f \u0627\u062d\u0633\u0627\u0633 \u06a9\u0633\u0644\u06cc \u0648 \u0628\u06cc\u200c\u062d\u0627\u0644\u06cc \u062f\u0627\u0631\u06cc\u062f\u061f<\/p>\n<!-- \/wp:paragraph -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            },
            {
                'id': 20,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:file {"href":"http:\/\/demo.laraberg.io\/photos\/shares\/2-river.jpeg"} -->\n<div class="wp-block-file"><a href="http:\/\/demo.laraberg.io\/photos\/shares\/2-river.jpeg" class="wp-block-file__button" download>Download<\/a><\/div>\n<!-- \/wp:file -->',
                'rendered_content': '<!-- wp:file {"href":"http:\/\/demo.laraberg.io\/photos\/shares\/2-river.jpeg"} -->\n<div class="wp-block-file"><a href="http:\/\/demo.laraberg.io\/photos\/shares\/2-river.jpeg" class="wp-block-file__button" download>Download<\/a><\/div>\n<!-- \/wp:file -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-06-27 20:02:27',
                'updated_at': '2019-06-27 20:02:27',
                'content': {
                    'raw': '<!-- wp:file {"href":"http:\/\/demo.laraberg.io\/photos\/shares\/2-river.jpeg"} -->\n<div class="wp-block-file"><a href="http:\/\/demo.laraberg.io\/photos\/shares\/2-river.jpeg" class="wp-block-file__button" download>Download<\/a><\/div>\n<!-- \/wp:file -->',
                    'rendered': '<!-- wp:file {"href":"http:\/\/demo.laraberg.io\/photos\/shares\/2-river.jpeg"} -->\n<div class="wp-block-file"><a href="http:\/\/demo.laraberg.io\/photos\/shares\/2-river.jpeg" class="wp-block-file__button" download>Download<\/a><\/div>\n<!-- \/wp:file -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            },
            {
                'id': 21,
                'raw_title': 'Untitled Reusable Block',
                'raw_content': '<!-- wp:heading {"align":"right"} -->\n<h2 style="text-align:right">\u0628\u0644\u0627 \u062a\u0644\u0641\u0638 \u0645\u06cc \u06af\u0631\u062f\u062f.<br>  \u062f\u0631 \u0634\u06a9\u0644 \u0632\u06cc\u0631 \u062d\u0631\u0648\u0641 \u0634\u06a9\u0644 \u0622\u0646\u0647\u0627 \u0648 \u0639\u0644\u0627\u0645\u062a \u0641\u0646\u0648\u062a\u06cc\u06a9 \u0622\u0648\u0631\u062f\u0647 \u0634\u062f\u0647 \u0627\u0633\u062a<\/h2>\n<!-- \/wp:heading -->',
                'rendered_content': '<!-- wp:heading {"align":"right"} -->\n<h2 style="text-align:right">\u0628\u0644\u0627 \u062a\u0644\u0641\u0638 \u0645\u06cc \u06af\u0631\u062f\u062f.<br>  \u062f\u0631 \u0634\u06a9\u0644 \u0632\u06cc\u0631 \u062d\u0631\u0648\u0641 \u0634\u06a9\u0644 \u0622\u0646\u0647\u0627 \u0648 \u0639\u0644\u0627\u0645\u062a \u0641\u0646\u0648\u062a\u06cc\u06a9 \u0622\u0648\u0631\u062f\u0647 \u0634\u062f\u0647 \u0627\u0633\u062a<\/h2>\n<!-- \/wp:heading -->',
                'status': 'publish',
                'slug': 'untitled-reusable-block',
                'type': 'wp_block',
                'created_at': '2019-06-27 21:02:33',
                'updated_at': '2019-06-27 21:02:33',
                'content': {
                    'raw': '<!-- wp:heading {"align":"right"} -->\n<h2 style="text-align:right">\u0628\u0644\u0627 \u062a\u0644\u0641\u0638 \u0645\u06cc \u06af\u0631\u062f\u062f.<br>  \u062f\u0631 \u0634\u06a9\u0644 \u0632\u06cc\u0631 \u062d\u0631\u0648\u0641 \u0634\u06a9\u0644 \u0622\u0646\u0647\u0627 \u0648 \u0639\u0644\u0627\u0645\u062a \u0641\u0646\u0648\u062a\u06cc\u06a9 \u0622\u0648\u0631\u062f\u0647 \u0634\u062f\u0647 \u0627\u0633\u062a<\/h2>\n<!-- \/wp:heading -->',
                    'rendered': '<!-- wp:heading {"align":"right"} -->\n<h2 style="text-align:right">\u0628\u0644\u0627 \u062a\u0644\u0641\u0638 \u0645\u06cc \u06af\u0631\u062f\u062f.<br>  \u062f\u0631 \u0634\u06a9\u0644 \u0632\u06cc\u0631 \u062d\u0631\u0648\u0641 \u0634\u06a9\u0644 \u0622\u0646\u0647\u0627 \u0648 \u0639\u0644\u0627\u0645\u062a \u0641\u0646\u0648\u062a\u06cc\u06a9 \u0622\u0648\u0631\u062f\u0647 \u0634\u062f\u0647 \u0627\u0633\u062a<\/h2>\n<!-- \/wp:heading -->'
                },
                'title': { 'raw': 'Untitled Reusable Block' }
            }]);
    })


    // Give nuxt middleware to express
    app.use(nuxt.render);

    // Listen the server
    app.listen(port, host)
    consola.ready({
        message: `Server listening on http://${ host }:${ port }${config.router.base}`,
        badge: true
    })
}

start()
