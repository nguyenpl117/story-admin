import {mount} from '@vue/test-utils';
import Logo from '@/components/logo/Logo.vue';

describe('Logo', () => {
    test('is a Vue instance', () => {
        const wrapper = mount(Logo);
        expect(wrapper.attributes('class')).toBe('VueToNuxtLogo');
    });
});
