const uuid = require('uuid');

export class Str {
    static uuid() {
        return uuid.v4();
    }
}