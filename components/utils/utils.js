import _ from 'lodash';

export function showNotifyGraphQL(graphQLErrors, callback) {
    if ( ! Array.isArray(graphQLErrors) ) {
        return;
    }

    let errors = graphQLErrors[0].validation || {};

    callback && callback(errors);

    if ( ! Object.keys(errors).length ) {
        return this.$notify({
            group: 'app',
            type: 'error',
            title: 'Error!',
            text: graphQLErrors[0].message,
            duration: 5000,
        });
    }

    for( const i in errors ) {
        let error = errors[i];
        for( let message of error ) {
            return this.$notify({
                group: 'app',
                type: 'error',
                title: 'Validate error!',
                text: message,
                duration: 5000,
            });
        }
    }
}

export function deleteFetch(mutation, id, refetchQueries) {
    return this.$apollo.mutate({
        mutation: mutation,
        variables: { id: id },
        refetchQueries: refetchQueries
    }).then(({ data }) => {
        for( let i in data ) {
            let item = data[i];
            if ( item.status ) {
                this.$notify({
                    group: 'app',
                    type: 'success',
                    title: 'Success',
                    text: 'Thao tác thành công.',
                    duration: 5000,
                });
            } else {
                this.$notify({
                    group: 'app',
                    type: 'error',
                    title: 'Error!',
                    text: item.message,
                    duration: 5000,
                });
            }
        }

    }).catch(({ graphQLErrors }) => {
        showNotifyGraphQL.bind(this)(graphQLErrors, (errors) => {
            // this.errors = errors;
        });
    });
}

export function createFetch({ mutation, variables, refetchQueries = [], options = {} }, callback) {
    let loading = this;
    let error = this;
    if ( options.hasOwnProperty('loading') ) {
        loading = options;
    }
    if ( options.hasOwnProperty('errors') ) {
        error = options;
    }

    if ( loading.loading ) {
        return;
    }

    loading.loading = true;

    return this.$apollo.mutate({
        mutation,
        variables,
        refetchQueries
    }).then(({ data }) => {
        if ( options.hidenNotification !== false ) {
            this.$notify({
                group: 'app',
                type: 'success',
                title: 'Success',
                text: 'Thao tác thành công.',
                duration: 2000,
            });
        }
        callback && callback(data);
    }).catch(({ graphQLErrors }) => {
        showNotifyGraphQL.bind(this)(graphQLErrors, (errors) => {
            error.errors = errors;
        });
    }).finally(() => {
        loading.loading = false;
    });
}

export function updateFetch({ mutation, variables, refetchQueries = [], options = {} }, callback) {
    let loading = this;
    let error = this;
    if ( options.hasOwnProperty('loading') ) {
        loading = options;
    }
    if ( options.hasOwnProperty('errors') ) {
        error = options;
    }

    if ( loading.loading ) return;

    loading.loading = true;

    return this.$apollo.mutate({
        mutation,
        variables,
        refetchQueries,
        awaitRefetchQueries: true
    }).then(({ data }) => {
        this.$notify({
            group: 'app',
            type: 'success',
            title: 'Success',
            text: 'Thao tác thành công.',
            duration: 5000,
        });
        callback && callback(data);
    }).catch((e) => {
        const { graphQLErrors } = e;
        showNotifyGraphQL.bind(this)(graphQLErrors, (errors) => {
            error.errors = errors;
            if ( this.hasOwnProperty('messageError') ) {
                this.messageError = graphQLErrors[0].message;
            }
        });
    }).finally(() => {
        loading.loading = false;
    });
}

export function convertStringToNumber(value) {
    if ( ! value ) {
        return 0;
    }
    value = (value + '').replace(/[^0-9+\-Ee.]/g, '');
    return parseFloat(value) || 0;
}

export function formatValues(object, keys, fn) {
    for( let i of keys ) _.update(object, i, fn);
    return object;
}

export function enMeta(form, meta) {
    if (!Array.isArray(meta)) {
        return;
    }
    meta.forEach(item => {
        form[`meta_${ item.metaKey }`] = item.metaValue;
    });
}

export function deMeta(_form) {
    let meta = [];
    for( let i in _form ) {
        let split = i.split('meta_');
        let isMeta = /^(meta\_).*/g.test(i);

        if ( isMeta ) {
            let value = _form[i];
            if (typeof value !== "string") {
                meta.push({
                    metaKey: split[1],
                    metaValue: JSON.stringify(value)
                });
            } else {
                meta.push({
                    metaKey: split[1],
                    metaValue: _form[i]
                });
            }
        }
    }

    return meta;
}

export function arrayMove(arr, oldIndex, newIndex) {
    arr = _.cloneDeep(arr);

    if ( newIndex >= arr.length ) {
        var k = newIndex - arr.length + 1;
        while ( k-- ) {
            arr.push(undefined);
        }
    }

    arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);

    return arr.map((x, i) => {
        return {
            ...x,
            order: i
        }
    });
}

export function categoriesRecursive(data, parentKey = 'parentId', idKey = 'id', childrenKey = 'children') {
    if ( ! Array.isArray(data) ) {
        return [];
    }

    let n = {};
    for( let item of data ) {
        if ( ! n[`${ item[parentKey] || 0 }-key`] ) n[`${ item[parentKey] || 0 }-key`] = [];
        n[`${ item[parentKey] || 0 }-key`].push(item);
    }
    for( let item of data ) {
        if ( n[item[idKey] + '-key'] ) {
            item[childrenKey] = n[item[idKey] + '-key'];
        }
    }
    const res = n['0-key'] || [];

    return converMenuItem(res);
}

export function converMenuItem(data, level = 0) {
    let menuItems = [];
    for( let item of data ) {

        let space = Array.from(Array(level)).map(x => ('\xA0\xA0\xA0\xA0')).join('');

        let childString = '';

        if ( level > 0 ) {
            childString = '|--\xA0\xA0';
        }

        menuItems.push({
            ...item,
            name: space + childString + item.name
        });

        if ( Array.isArray(item.children) ) {
            menuItems.push.apply(menuItems, converMenuItem(item.children, level + 1));
        }
    }
    return menuItems;
}

export function isNullOrUndefined(value) {
    return value === null || value === void (0);
}

export class Cart {
    constructor(data, callback) {
        this.callback = callback;
        this.items = [];
        this.discount = 0;
        this.discountType = 1;
        this.shipping = {
            id: undefined,
            methodId: undefined,
            name: undefined,
            price: 0
        }
        this.taxes = true;
        this.taxValue = 10;

        this.shippingAddress = {};
        this.billingAddress = {};

        if ( data instanceof Cart ) {
            // this.items = data.items;
            // this.callback = data.callback;
            for( let i in data ) {
                this[i] = data[i];
            }
        } else if ( Array.isArray(data) ) {
            for( let item of data ) {
                this.push(item);
            }
        } else {
            this.push(data);
        }
    }

    multiple(data) {
        if ( Array.isArray(data) ) {
            for( let item of data ) {
                this.push(item);
            }
        } else {
            this.push(data);
        }
        return this;
    }

    push(cartItem) {
        if ( ! cartItem ) {
            return;
        }
        if ( cartItem instanceof CartItem ) {
            this.items.push(cartItem);
        } else {
            if ( ! cartItem.quantity ) {
                cartItem.quantity = 1;
            }
            this.items.push(new CartItem(cartItem, this.callback));
        }
    }

    getQuantity() {
        const reduce = (amount, value) => (amount + value.getQuantity());
        return this.items.reduce(reduce, 0);
    }

    getTotal() {
        return this.getSubtotal() + this.getTax() + this.getPriceShipping();
    }

    getTax() {
        if ( ! this.taxes ) {
            return 0;
        }

        return this.getSubtotal() * this.taxValue / 100;
    }

    getSubtotal() {
        return (this.getTotalOrigin() - this.getDiscountValue());
    }

    getTotalOrigin() {
        const reduce = (amount, value) => (amount + value.getTotal());
        return this.items.reduce(reduce, 0);
    }

    getDiscountValue() {
        if ( this.discountType === 1 ) {
            return this.getDiscount();
        }
        return this.getTotalOrigin() * this.getDiscount() / 100;
    }

    getDiscount() {
        if ( this.discount > this.getTotalOrigin() ) {
            return this.getTotalOrigin();
        }

        return this.discount || 0;
    }

    setDiscount({ discount, discountType }) {
        this.discountType = discountType;
        this.discount = convertStringToNumber(discount);
    }

    getPriceShipping() {
        return convertStringToNumber(this.shipping.price);
    }

    setShipping(shipping) {
        this.shipping = shipping;
    }

    resetShipping() {
        this.shipping = {
            id: undefined,
            methodId: undefined,
            name: undefined,
            price: 0
        }
    }

    resetTaxes() {
        this.taxes = true;
    }

    setTaxes(taxes) {
        this.taxes = taxes;
    }
}

class CartItem {
    constructor(data, callback) {
        this.discount = 0;
        this.discountType = 1;
        this.price = 0;

        for( let i in data ) {
            this[i] = data[i];
        }

        this.de = _.debounce((name, qty) => {
            callback && callback();
            this[name] = qty;
        }, 300);
    }

    getId() {
        return String(this.id);
    }

    getImage() {
        return _.get(this, 'images.0.image', null) || this.image;
    }

    getName() {
        return this.fullname || this.name;
    }

    getQuantity() {
        return convertStringToNumber(this.quantity);
    }

    setQuantity(qty) {
        this.de('quantity', qty);
    }

    setDiscount({ discount, discountType }) {
        this.discountType = discountType;
        this.discount = convertStringToNumber(discount);
        // this.de('discount', convertStringToNumber(discount))
    }

    getDiscount() {
        return convertStringToNumber(this.discount);
    }

    getDiscountValue() {
        if ( this.discountType === 1 ) {
            return this.getDiscount();
        }
        return this.getPrice() * this.getDiscount() / 100;
    }

    getPrice() {
        return convertStringToNumber(this.price);
    }

    getPriceSell() {
        let sell = convertStringToNumber(this.price) - this.getDiscountValue();
        return sell < 0 ? 0 : sell;
    }

    getTotal() {
        try {
            return (this.getPriceSell() * this.getQuantity());
        } catch (e) {
            return 0;
        }
    }
}