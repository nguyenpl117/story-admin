const uuid = require('uuid');

class FormBuilder {
    textField({ label = 'Heading', name = 'heading', attrs }) {
        return {
            id: uuid.v4(),
            type: 'TextField',
            label,
            name,
            attrs
        };
    }

    categoryPost({ label = 'Heading', name = 'heading', attrs }) {
        return {
            id: uuid.v4(),
            type: 'categoryPost',
            label,
            name,
            attrs
        };
    }

    categoryProduct({ label = 'Heading', name = 'heading', attrs }) {
        return {
            id: uuid.v4(),
            type: 'categoryProduct',
            label,
            name,
            attrs
        };
    }

    menu({ label = 'Heading', name = 'heading', attrs }) {
        return {
            id: uuid.v4(),
            type: 'menu',
            label,
            name,
            attrs
        };
    }

    image({ label = 'Heading', name = 'heading', attrs }) {
        return {
            id: uuid.v4(),
            type: 'image',
            label,
            name,
            attrs
        };
    }

    checkbox({ label = 'Heading', name = 'heading', attrs }) {
        return {
            id: uuid.v4(),
            type: 'checkbox',
            label,
            name,
            attrs
        };
    }

    select({ label = 'Heading', name = 'heading', options = [], attrs }) {
        return {
            id: uuid.v4(),
            type: 'select',
            label,
            name,
            options,
            attrs
        };
    }

    slider({
               header = '', label = '{counter} slider', name = 'heading', attrs = {
            min: 3,
            max: 10
        }
           }) {
        return {
            id: uuid.v4(),
            type: 'slider',
            header,
            label,
            name,
            attrs
        };
    }

    text({ label = 'Heading', name = 'heading', attrs }) {
        return {
            id: uuid.v4(),
            type: 'text',
            label,
            name,
            attrs
        };
    }

    color({ label = 'Heading', name = 'heading', items, attrs }) {
        return {
            id: uuid.v4(),
            type: 'color',
            label,
            name,
            items,
            attrs
        };
    }

    listItem({
                 label = 'Heading', name = 'heading', icon, svg, title =
            item => {
            }, data = {}, items = [], attrs
             }) {
        return {
            id: uuid.v4(),
            type: 'listItem',
            label,
            name,
            svg,
            icon,
            title,
            data,
            items,
            attrs
        };
    }
}

const Builder = new FormBuilder();

export const LIST_SECTION = [
    {
        id: uuid.v4(),
        name: 'Post',
        children: [
            {
                id: uuid.v4(),
                name: 'Blog post',
                type: 'post',
                icon: 'mdi-file-document-edit-outline',
                configs: {
                    heading: 'Posts',
                    rows: 4,
                    perRow: 2,
                    showDate: true,
                    showAuthor: false,
                    categoryId: '0',
                    isVideo: true
                },
                themes: [
                    Builder.textField({
                        label: 'Heading',
                        name: 'heading'
                    }),
                    Builder.categoryPost({
                        label: 'Chuyên mục',
                        name: 'categoryId'
                    }),
                    Builder.slider({
                        header: 'Rows',
                        label: '{counter}',
                        name: 'rows',
                        attrs: {
                            min: 2,
                            max: 5
                        }
                    }),
                    Builder.slider({
                        header: 'Post per row',
                        label: '{counter}',
                        name: 'perRow',
                        attrs: {
                            min: 1,
                            max: 4
                        }
                    }),
                    Builder.checkbox(
                        { label: 'Show author', name: 'showAuthor' }),
                    Builder.checkbox({ label: 'Show date', name: 'showDate' }),
                    Builder.checkbox({ label: 'Is video', name: 'isVideo' })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Featured post',
                type: 'featured-post',
                icon: 'mdi-file-document-edit-outline',
                configs: {
                    heading: 'Featured Posts',
                    rows: 4,
                    perRow: 2,
                    showDate: true,
                    showAuthor: false,
                    categoryId: '0'
                },
                themes: [
                    Builder.textField({
                        label: 'Heading',
                        name: 'heading'
                    }),
                    Builder.categoryPost({
                        label: 'Chuyên mục',
                        name: 'categoryId'
                    }),
                    Builder.slider({
                        header: 'Rows',
                        label: '{counter}',
                        name: 'rows',
                        attrs: {
                            min: 2,
                            max: 5
                        }
                    }),
                    Builder.slider({
                        header: 'Post per row',
                        label: '{counter}',
                        name: 'perRow',
                        attrs: {
                            min: 1,
                            max: 4
                        }
                    }),
                    Builder.checkbox(
                        { label: 'Show author', name: 'showAuthor' }),
                    Builder.checkbox({ label: 'Show date', name: 'showDate' })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Latest post',
                type: 'latest-post',
                icon: 'mdi-file-document-edit-outline',
                configs: {
                    heading: 'Latest Posts',
                    rows: 4,
                    perRow: 2,
                    showDate: true,
                    categoryId: '0'
                },
                themes: [
                    Builder.textField({
                        label: 'Heading',
                        name: 'heading'
                    }),
                    Builder.categoryPost({
                        label: 'Chuyên mục',
                        name: 'categoryId'
                    }),
                    Builder.slider({
                        header: 'Rows',
                        label: '{counter}',
                        name: 'rows',
                        attrs: {
                            min: 2,
                            max: 5
                        }
                    }),
                    Builder.slider({
                        header: 'Post per row',
                        label: '{counter}',
                        name: 'perRow',
                        attrs: {
                            min: 1,
                            max: 4
                        }
                    }),
                    Builder.checkbox(
                        { label: 'Show author', name: 'showAuthor' }),
                    Builder.checkbox({ label: 'Show date', name: 'showDate' })
                ]
            }
        ]
    },
    {
        id: uuid.v4(),
        name: 'Image',
        children: [
            {
                id: uuid.v4(),
                name: 'Gallery',
                type: 'gallery',
                icon: 'mdi-image-outline',
                configs: {
                    height: 'medium',
                    contents: [
                        {
                            id: uuid.v4(),
                            image: {},
                            link: '',
                            align: 'middle'
                        },
                        {
                            id: uuid.v4(),
                            image: {},
                            link: '',
                            align: 'middle'
                        },
                        {
                            id: uuid.v4(),
                            image: {},
                            link: '',
                            align: 'middle'
                        }
                    ]
                },
                themes: [
                    Builder.select({
                        label: 'Section height',
                        name: 'height',
                        options: [
                            {
                                value: 'small',
                                text: 'Small'
                            },
                            {
                                value: 'medium',
                                text: 'Medium'
                            },
                            {
                                value: 'large',
                                text: 'Large'
                            },
                            {
                                value: 'x-large',
                                text: 'Extra Large'
                            }
                        ]
                    }),
                    Builder.listItem({
                        label: 'Content',
                        name: 'contents',
                        icon: 'photo',
                        title: ('Image'),
                        data: {
                            image: {},
                            link: '',
                            align: 'middle'
                        },
                        items: [
                            Builder.image({
                                label: 'Image',
                                name: 'image'
                            }),
                            Builder.select({
                                label: 'Image alignment',
                                name: 'align',
                                options: [
                                    {
                                        value: 'top',
                                        text: 'Top'
                                    },
                                    {
                                        value: 'middle',
                                        text: 'Middle'
                                    },
                                    {
                                        value: 'bottom',
                                        text: 'Bottom'
                                    }
                                ]
                            }),
                            Builder.textField({
                                label: 'Link',
                                name: 'link',
                                attrs: {
                                    hint: 'Optional'
                                }
                            }),
                            Builder.textField({
                                label: 'Caption',
                                name: 'caption'
                            })

                        ]
                    })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Image with text',
                type: 'image-with-text',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>image-with-text</title><path d="M9 8H1c-.552 0-1-.448-1-1V1c0-.552.448-1 1-1h8c.552 0 1 .448 1 1v6c0 .552-.448 1-1 1zM2 6h6V2H2v4zm7 14H1c-.552 0-1-.448-1-1s.448-1 1-1h8c.552 0 1 .448 1 1s-.448 1-1 1zm10-4H1c-.552 0-1-.448-1-1s.448-1 1-1h18c.552 0 1 .448 1 1s-.448 1-1 1zm0-4H1c-.552 0-1-.448-1-1s.448-1 1-1h18c.552 0 1 .448 1 1s-.448 1-1 1zm0-4h-6c-.552 0-1-.448-1-1s.448-1 1-1h6c.552 0 1 .448 1 1s-.448 1-1 1zm0-4h-6c-.552 0-1-.448-1-1s.448-1 1-1h6c.552 0 1 .448 1 1s-.448 1-1 1z"></path></svg>',
                configs: {
                    image: {},
                    align: 'left',
                    heading: 'Image with text',
                    text: 'Pair large text with an image to give focus to your chosen product, collection, or blog post. Add details on availability, style, or even provide a review.',
                    buttonLabel: '',
                    buttonLink: ''
                },
                themes: [
                    Builder.image({
                        label: 'Image',
                        name: 'image'
                    }),
                    Builder.select({
                        label: 'Image alignment', name: 'align', options: [
                            {
                                value: 'left',
                                text: 'Left'
                            },
                            {
                                value: 'right',
                                text: 'Right'
                            }
                        ]
                    }),
                    Builder.textField({ label: 'Heading', name: 'heading' }),
                    Builder.text({ label: 'Text', name: 'text' }),
                    Builder.textField(
                        { label: 'Button label', name: 'buttonLabel' }),
                    Builder.textField(
                        { label: 'Button link', name: 'buttonLink' })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Image with text overlay',
                type: 'image-with-text-overlay',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>image-with-text</title><path d="M19 4h-6c-.553 0-1 .448-1 1s.447 1 1 1h5v4.586l-1.293-1.293c-.39-.39-1.023-.39-1.414 0l-4.42 4.42-2.318-1.545c-.336-.225-.774-.225-1.11 0L6 13.132V13c0-.553-.447-1-1-1s-1 .447-1 1v6c0 .553.447 1 1 1h14c.553 0 1-.447 1-1V5c0-.552-.447-1-1-1zM6 18v-2.465l2-1.333 2.445 1.63c.395.266.925.213 1.262-.125L16 11.414l2 2V18H6zm3-8.5c0 .83.672 1.5 1.5 1.5s1.5-.67 1.5-1.5S11.328 8 10.5 8 9 8.67 9 9.5zM1 2h8c.553 0 1-.448 1-1s-.447-1-1-1H1C.447 0 0 .448 0 1s.447 1 1 1zm0 4h8c.553 0 1-.448 1-1s-.447-1-1-1H1c-.553 0-1 .448-1 1s.447 1 1 1zm6 3c0-.552-.447-1-1-1H1c-.553 0-1 .448-1 1s.447 1 1 1h5c.553 0 1-.448 1-1z"></path></svg>',
                configs: {
                    image: {},
                    align: 'middle',
                    layout: 'full',
                    height: 'medium',
                    textSize: 'medium',
                    heading: 'Image with text overlay',
                    text: 'Use overlay text to give your customers insight into your brand. Select imagery and text that relates to your style and story.',
                    buttonLabel: '',
                    buttonLink: ''
                },
                themes: [
                    Builder.image({ label: 'Image', name: 'image' }),
                    Builder.select({
                        label: 'Image alignment', name: 'align', options: [
                            {
                                value: 'top',
                                text: 'Top'
                            },
                            {
                                value: 'middle',
                                text: 'Middle'
                            },
                            {
                                value: 'bottom',
                                text: 'Bottom'
                            }
                        ]
                    }),
                    Builder.select({
                        label: 'layout', name: 'layout', options: [
                            {
                                value: 'full',
                                text: 'Full width'
                            },
                            {
                                value: 'fixed',
                                text: 'Fixed width'
                            }
                        ]
                    }),
                    Builder.select({
                        label: 'Section height', name: 'height', options: [
                            {
                                value: 'adapt',
                                text: 'Adapt to image'
                            },
                            {
                                value: 'small',
                                text: 'Small'
                            },
                            {
                                value: 'medium',
                                text: 'Medium'
                            },
                            {
                                value: 'large',
                                text: 'Large'
                            },
                            {
                                value: 'x-large',
                                text: 'Extra Large'
                            }
                        ]
                    }),
                    Builder.select({
                        label: 'Text size', name: 'textSize', options: [
                            {
                                value: 'medium',
                                text: 'Medium'
                            },
                            {
                                value: 'large',
                                text: 'Large'
                            }
                        ]
                    }),
                    Builder.textField({ label: 'Heading', name: 'heading' }),
                    Builder.text({ label: 'Text', name: 'text' }),
                    Builder.textField(
                        { label: 'Button label', name: 'buttonLabel' }),
                    Builder.textField(
                        { label: 'Button link', name: 'buttonLink' })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Logo list',
                type: 'logo-list',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>logo-list</title><path d="M16 6c-1.2 0-2.267.542-3 1.382C12.267 6.542 11.2 6 10 6s-2.266.542-3 1.382C6.266 6.542 5.2 6 4 6c-2.206 0-4 1.794-4 4s1.794 4 4 4c1.2 0 2.266-.542 3-1.382.734.84 1.8 1.382 3 1.382s2.267-.542 3-1.382c.733.84 1.8 1.382 3 1.382 2.206 0 4-1.794 4-4s-1.794-4-4-4zm0 6c-1.103 0-2-.897-2-2s.897-2 2-2 2 .897 2 2-.897 2-2 2zm-6 0c-1.103 0-2-.897-2-2s.897-2 2-2 2 .897 2 2-.897 2-2 2zm-6 0c-1.103 0-2-.897-2-2s.897-2 2-2 2 .897 2 2-.897 2-2 2zm-2 6c0-.553-.448-1-1-1s-1 .447-1 1v1c0 .553.448 1 1 1h1c.552 0 1-.447 1-1s-.448-1-1-1zm17-1c-.553 0-1 .447-1 1-.553 0-1 .447-1 1s.447 1 1 1h1c.553 0 1-.447 1-1v-1c0-.553-.447-1-1-1zm0-17h-1c-.553 0-1 .448-1 1s.447 1 1 1c0 .552.447 1 1 1s1-.448 1-1V1c0-.552-.447-1-1-1zM1 3c.552 0 1-.448 1-1 .552 0 1-.448 1-1s-.448-1-1-1H1C.448 0 0 .448 0 1v1c0 .552.448 1 1 1zm5-1h2c.552 0 1-.448 1-1s-.448-1-1-1H6c-.552 0-1 .448-1 1s.448 1 1 1zm6 0h2c.553 0 1-.448 1-1s-.447-1-1-1h-2c-.553 0-1 .448-1 1s.447 1 1 1zM8 18H6c-.552 0-1 .447-1 1s.448 1 1 1h2c.552 0 1-.447 1-1s-.448-1-1-1zm6 0h-2c-.553 0-1 .447-1 1s.447 1 1 1h2c.553 0 1-.447 1-1s-.447-1-1-1z"></path></svg>',
                configs: {
                    heading: 'Logo list',
                    contents: [
                        {
                            id: uuid.v4(),
                            image: {},
                            link: ''
                        },
                        {
                            id: uuid.v4(),
                            image: {},
                            link: ''
                        },
                        {
                            id: uuid.v4(),
                            image: {},
                            link: ''
                        },
                        {
                            id: uuid.v4(),
                            image: {},
                            link: ''
                        }
                    ]
                },
                themes: [
                    Builder.textField({ label: 'Heading', name: 'heading' }),
                    Builder.listItem({
                        label: 'Content',
                        name: 'contents',
                        icon: 'photo',
                        title: ('Logo'),
                        data: {},
                        items: [
                            Builder.image({ label: 'Logo', name: 'image' }),
                            Builder.textField({ label: 'Link', name: 'link' })
                        ]
                    })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Slideshow',
                type: 'slideshow',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>slideshow</title><path d="M19 0H1C.447 0 0 .448 0 1v14c0 .552.447 1 1 1h18c.553 0 1-.448 1-1V1c0-.552-.447-1-1-1zm-1 2v4.92l-3.375-2.7c-.397-.32-.972-.288-1.332.073l-4.42 4.42-2.318-1.545c-.324-.216-.738-.225-1.07-.025L2 9.233V2h16zM2 14v-2.434l3.972-2.383 2.473 1.65c.396.263.924.212 1.262-.126l4.367-4.367L18 9.48V14H2zm4 4c-.553 0-1 .448-1 1s.447 1 1 1c.553 0 1-.448 1-1s-.447-1-1-1zm4 0c-.553 0-1 .448-1 1s.447 1 1 1c.553 0 1-.448 1-1s-.447-1-1-1zm4 0c-.553 0-1 .448-1 1s.447 1 1 1c.553 0 1-.448 1-1s-.447-1-1-1z"></path></svg>',
                configs: {
                    contents: [
                        {
                            id: uuid.v4(),
                            position: 'center',
                            heading: 'Image slide',
                            subHeading: 'Tell your brand\'s story through images',
                            label: '',
                            link: '',
                            image: {}
                        },
                        {
                            id: uuid.v4(),
                            position: 'center',
                            heading: 'Image slide',
                            subHeading: 'Tell your brand\'s story through images',
                            label: '',
                            link: '',
                            image: {}
                        }
                    ],
                    overlay: true,
                    hideDelimiters: false,
                    delay: 5,
                    height: 'medium',
                    fontSize: 'medium',
                    align: 'center top'
                },
                themes: [
                    Builder.select({
                        label: 'Slide height', name: 'height', options: [
                            {
                                text: 'Adapt to first image',
                                value: 'adapt'
                            },
                            {
                                text: 'Small',
                                value: 'small'
                            },
                            {
                                text: 'Medium',
                                value: 'medium'
                            },
                            {
                                text: 'Large',
                                value: 'large'
                            }
                        ]
                    }),
                    Builder.select({
                        label: 'Text size', name: 'fontSize', options: [
                            {
                                text: 'Medium',
                                value: 'medium'
                            },
                            {
                                text: 'Large',
                                value: 'large'
                            }
                        ]
                    }),
                    Builder.select({
                        label: 'Text alignment', name: 'align', options: [
                            {
                                text: 'Top left',
                                value: 'left top'
                            },
                            {
                                text: 'Top center',
                                value: 'center top'
                            },
                            {
                                text: 'Top right',
                                value: 'right top'
                            },
                            {
                                text: 'Middle left',
                                value: 'left middle'
                            },
                            {
                                text: 'Middle center',
                                value: 'center middle'
                            },
                            {
                                text: 'Middle right',
                                value: 'right middle'
                            },

                            {
                                text: 'Bottom left',
                                value: 'left bottom'
                            },
                            {
                                text: 'Bottom center',
                                value: 'center bottom'
                            },
                            {
                                text: 'Bottom right',
                                value: 'right bottom'
                            }
                        ]
                    }),
                    Builder.checkbox(
                        { label: 'Hide delimiters', name: 'hideDelimiters' }),
                    Builder.slider({
                        header: 'Change slides every',
                        label: '{counter}s',
                        name: 'delay',
                        attrs: { min: 3, max: 9 }
                    }),
                    Builder.listItem({
                        label: 'Contents',
                        name: 'contents',
                        icon: 'photo',
                        title: ('Image slide'),
                        data: {
                            position: 'center',
                            heading: 'Image slide',
                            subHeading: 'Tell your brand\'s story through images',
                            label: '',
                            link: '',
                            image: {}
                        },
                        items: [
                            Builder.image({ label: 'Image', name: '' }),
                            Builder.select({
                                label: 'Image', name: '', options: [
                                    {
                                        text: 'Center',
                                        value: 'center'
                                    }
                                ]
                            }),
                            Builder.textField(
                                { label: 'Heading', name: 'heading' }),
                            Builder.textField(
                                { label: 'Subheading', name: 'subHeading' }),
                            Builder.textField(
                                { label: 'Button label', name: 'label' }),
                            Builder.textField(
                                { label: 'Button link', name: 'link' })
                        ]
                    })
                ]
            }

        ]
    },
    {
        id: uuid.v4(),
        name: 'Product',
        children: [
            {
                id: uuid.v4(),
                name: 'Product',
                type: 'product',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>collections-list</title><path fill="currentColor" d="M7 15h12v4H7v-4z"></path><path d="M19 20c.553 0 1-.447 1-1V6c0-.297-.132-.578-.36-.77l-6-5c-.37-.307-.91-.307-1.28 0L10 2.198 7.64.23c-.37-.307-.91-.307-1.28 0l-6 5C.13 5.423 0 5.704 0 6v13c0 .553.447 1 1 1h18zM8 18v-2h10v2H8zm-6 0v-2h4v2H2zM7 2.302L8.438 3.5l-2.08 1.73C6.133 5.423 6 5.704 6 6v8H2V6.47L7 2.3zm6 0l5 4.167V14H8V6.47l5-4.17zM13 7c.553 0 1-.447 1-1s-.447-1-1-1c-.553 0-1 .447-1 1s.447 1 1 1z"></path></svg>',
                configs: {
                    heading: 'Products',
                    rows: 4,
                    perRow: 2,
                    showVendor: false,
                    showViewAll: false,
                    categoryId: '0'
                },
                themes: [
                    Builder.textField({ label: 'Heading', name: 'heading' }),
                    Builder.categoryProduct(
                        { label: 'Danh mục', name: 'categoryId' }),
                    Builder.slider({
                        header: 'Rows',
                        label: '{counter}',
                        name: 'rows',
                        attrs: { min: 2, max: 5 }
                    }),
                    Builder.slider({
                        header: 'Product per row',
                        label: '{counter}',
                        name: 'perRow',
                        attrs: { min: 1, max: 4 }
                    }),
                    Builder.checkbox(
                        { label: 'Show product vendor', name: 'showVendor' }),
                    Builder.checkbox({
                        label: 'Show \'view all\' button',
                        name: 'showViewAll'
                    })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Featured product',
                type: 'featured-product',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>products</title><path d="M19 0h-9c-.265 0-.52.106-.707.293l-9 9c-.39.39-.39 1.023 0 1.414l9 9c.195.195.45.293.707.293s.512-.098.707-.293l9-9c.188-.187.293-.442.293-.707V1c0-.552-.448-1-1-1zm-9 17.586L2.414 10 4 8.414 11.586 16 10 17.586zm8-8l-5 5L5.414 7l5-5H18v7.586zM15 6c.552 0 1-.448 1-1s-.448-1-1-1-1 .448-1 1 .448 1 1 1z"></path></svg>',
                configs: {
                    heading: 'Featured product',
                    rows: 4,
                    perRow: 2,
                    showVendor: false,
                    showViewAll: false,
                    categoryId: '0'
                },
                themes: [
                    Builder.textField({ label: 'Heading', name: 'heading' }),
                    Builder.categoryProduct(
                        { label: 'Danh mục', name: 'categoryId' }),
                    Builder.slider({
                        header: 'Rows',
                        label: '{counter}',
                        name: 'rows',
                        attrs: { min: 2, max: 5 }
                    }),
                    Builder.slider({
                        header: 'Product per row',
                        label: '{counter}',
                        name: 'perRow',
                        attrs: { min: 1, max: 4 }
                    }),
                    Builder.checkbox(
                        { label: 'Show product vendor', name: 'showVendor' }),
                    Builder.checkbox({
                        label: 'Show \'view all\' button',
                        name: 'showViewAll'
                    })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Latest product',
                type: 'latest-product',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>products</title><path d="M19 0h-9c-.265 0-.52.106-.707.293l-9 9c-.39.39-.39 1.023 0 1.414l9 9c.195.195.45.293.707.293s.512-.098.707-.293l9-9c.188-.187.293-.442.293-.707V1c0-.552-.448-1-1-1zm-9 17.586L2.414 10 4 8.414 11.586 16 10 17.586zm8-8l-5 5L5.414 7l5-5H18v7.586zM15 6c.552 0 1-.448 1-1s-.448-1-1-1-1 .448-1 1 .448 1 1 1z"></path></svg>',
                configs: {
                    heading: 'Latest product',
                    rows: 4,
                    perRow: 2,
                    showVendor: false,
                    showViewAll: false,
                    categoryId: '0'
                },
                themes: [
                    Builder.textField({ label: 'Heading', name: 'heading' }),
                    Builder.categoryProduct(
                        { label: 'Danh mục', name: 'categoryId' }),
                    Builder.slider({
                        header: 'Rows',
                        label: '{counter}',
                        name: 'rows',
                        attrs: { min: 2, max: 5 }
                    }),
                    Builder.slider({
                        header: 'Product per row',
                        label: '{counter}',
                        name: 'perRow',
                        attrs: { min: 1, max: 4 }
                    }),
                    Builder.checkbox(
                        { label: 'Show product vendor', name: 'showVendor' }),
                    Builder.checkbox({
                        label: 'Show \'view all\' button',
                        name: 'showViewAll'
                    })
                ]
            }
        ]
    },
    {
        id: uuid.v4(),
        name: 'STORE INFORMATION',
        children: [
            {
                id: uuid.v4(),
                name: 'Map',
                type: 'map',
                icon: 'mdi-map-marker-outline',
                configs: {},
                themes: []
            }
        ]
    },
    {
        id: uuid.v4(),
        name: 'Text',
        children: [
            {
                id: uuid.v4(),
                name: 'Rich text',
                type: 'rich-text',
                icon: 'mdi-text',
                configs: {
                    heading: 'Talk about your brand',
                    text: 'Use this text to share information about your brand with your customers. Describe a product, share announcements, or welcome customers to your store.',
                    wideDisplay: false,
                    size: 'medium'
                },
                themes: [
                    Builder.checkbox(
                        { label: 'Wide display', name: 'wideDisplay' }),
                    Builder.textField({ label: 'Heading', name: 'heading' }),
                    Builder.text({ label: 'Text', name: 'text' }),
                    Builder.select({
                        label: 'Size', name: 'size', options: [
                            {
                                value: 'small',
                                text: 'Small'
                            },
                            {
                                value: 'medium',
                                text: 'Medium'
                            },
                            {
                                value: 'large',
                                text: 'Large'
                            }
                        ]
                    })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Testimonials',
                type: 'testimonials',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>testimonials</title><path d="M7 4H6c-.553 0-1-.448-1-1V1c0-.552.447-1 1-1s1 .448 1 1v1c.553 0 1 .448 1 1s-.447 1-1 1zM2 4H1c-.553 0-1-.448-1-1V1c0-.552.447-1 1-1s1 .448 1 1v1c.553 0 1 .448 1 1s-.447 1-1 1zm12 16c-.553 0-1-.448-1-1v-1c-.553 0-1-.448-1-1s.447-1 1-1h1c.553 0 1 .448 1 1v2c0 .552-.447 1-1 1zm5 0c-.553 0-1-.448-1-1v-1c-.553 0-1-.448-1-1s.447-1 1-1h1c.553 0 1 .448 1 1v2c0 .552-.447 1-1 1zm0-6H1c-.553 0-1-.448-1-1s.447-1 1-1h18c.553 0 1 .448 1 1s-.447 1-1 1zM9 20H1c-.553 0-1-.448-1-1s.447-1 1-1h8c.553 0 1 .448 1 1s-.447 1-1 1zM19 2h-8c-.553 0-1-.448-1-1s.447-1 1-1h8c.553 0 1 .448 1 1s-.447 1-1 1zm0 6H1c-.553 0-1-.448-1-1s.447-1 1-1h18c.553 0 1 .448 1 1s-.447 1-1 1z"></path></svg>',
                configs: {
                    heading: 'Testimonials',
                    contents: [
                        {
                            id: uuid.v4(),
                            text: 'Add customer reviews and testimonials to showcase your store’s happy customers.',
                            author: 'Author\'s name'
                        },
                        {
                            id: uuid.v4(),
                            text: 'Add customer reviews and testimonials to showcase your store’s happy customers.',
                            author: 'Author\'s name'
                        },
                        {
                            id: uuid.v4(),
                            text: 'Add customer reviews and testimonials to showcase your store’s happy customers.',
                            author: 'Author\'s name'
                        }
                    ]
                },
                themes: [
                    Builder.textField({ label: 'heading', name: 'heading' }),
                    Builder.listItem({
                        label: 'Content', name: 'contents',
                        svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>testimonials</title><path d="M7 4H6c-.553 0-1-.448-1-1V1c0-.552.447-1 1-1s1 .448 1 1v1c.553 0 1 .448 1 1s-.447 1-1 1zM2 4H1c-.553 0-1-.448-1-1V1c0-.552.447-1 1-1s1 .448 1 1v1c.553 0 1 .448 1 1s-.447 1-1 1zm12 16c-.553 0-1-.448-1-1v-1c-.553 0-1-.448-1-1s.447-1 1-1h1c.553 0 1 .448 1 1v2c0 .552-.447 1-1 1zm5 0c-.553 0-1-.448-1-1v-1c-.553 0-1-.448-1-1s.447-1 1-1h1c.553 0 1 .448 1 1v2c0 .552-.447 1-1 1zm0-6H1c-.553 0-1-.448-1-1s.447-1 1-1h18c.553 0 1 .448 1 1s-.447 1-1 1zM9 20H1c-.553 0-1-.448-1-1s.447-1 1-1h8c.553 0 1 .448 1 1s-.447 1-1 1zM19 2h-8c-.553 0-1-.448-1-1s.447-1 1-1h8c.553 0 1 .448 1 1s-.447 1-1 1zm0 6H1c-.553 0-1-.448-1-1s.447-1 1-1h18c.553 0 1 .448 1 1s-.447 1-1 1z"></path></svg>',
                        title: ('Testimonials'),
                        data: {
                            text: 'Add customer reviews and testimonials to showcase your store’s happy customers.',
                            author: 'Author\'s name'
                        },
                        items: [
                            Builder.text({ label: 'Text', name: 'text' }),
                            Builder.textField(
                                { label: 'Tác giả', name: 'author' })
                        ]
                    })
                ]
            },
            {
                id: uuid.v4(),
                name: 'Text columns with images',
                type: 'text-columns-with-images',
                svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>text-column-with-images</title><path d="M19 0c.553 0 1 .448 1 1v6c0 .552-.447 1-1 1h-7c-.553 0-1-.448-1-1V1c0-.552.447-1 1-1h7zM8 0c.553 0 1 .448 1 1v6c0 .552-.447 1-1 1H1c-.553 0-1-.448-1-1V1c0-.552.447-1 1-1h7zm5 6h5V2h-5v4zM2 6h5V2H2v4zm6 4H1c-.553 0-1 .448-1 1s.447 1 1 1h7c.553 0 1-.448 1-1s-.447-1-1-1zm0 4H1c-.553 0-1 .448-1 1s.447 1 1 1h7c.553 0 1-.448 1-1s-.447-1-1-1zm-5 4H1c-.553 0-1 .448-1 1s.447 1 1 1h2c.553 0 1-.448 1-1s-.447-1-1-1zm16-8h-7c-.553 0-1 .448-1 1s.447 1 1 1h7c.553 0 1-.448 1-1s-.447-1-1-1zm0 4h-7c-.553 0-1 .448-1 1s.447 1 1 1h7c.553 0 1-.448 1-1s-.447-1-1-1zm-5 4h-2c-.553 0-1 .448-1 1s.447 1 1 1h2c.553 0 1-.448 1-1s-.447-1-1-1z"></path></svg>',
                configs: {
                    heading: 'Text columns with images',
                    contents: [
                        {
                            id: uuid.v4(),
                            heading: 'Add a title or tagline',
                            image: {},
                            text: 'Share blog posts, products, or promotions with your customers. Use this text to describe products, share details on availability and style, or as a space to display recent reviews or FAQs.',
                            buttonLabel: '',
                            buttonLink: '',
                            showImage: true
                        },
                        {
                            id: uuid.v4(),
                            heading: 'Add a title or tagline',
                            image: {},
                            text: 'Share blog posts, products, or promotions with your customers. Use this text to describe products, share details on availability and style, or as a space to display recent reviews or FAQs.',
                            buttonLabel: '',
                            buttonLink: '',
                            showImage: true
                        },
                        {
                            id: uuid.v4(),
                            heading: 'Add a title or tagline',
                            image: {},
                            text: 'Share blog posts, products, or promotions with your customers. Use this text to describe products, share details on availability and style, or as a space to display recent reviews or FAQs.',
                            buttonLabel: '',
                            buttonLink: '',
                            showImage: true
                        }

                    ]
                },
                themes: [
                    Builder.textField({ label: 'heading', name: 'heading' }),
                    Builder.listItem({
                        label: 'Content', name: 'contents',
                        icon: 'mdi-text',
                        title: '{heading}',
                        data: {
                            heading: 'Add a title or tagline',
                            image: {},
                            text: 'Share blog posts, products, or promotions with your customers. Use this text to describe products, share details on availability and style, or as a space to display recent reviews or FAQs.',
                            buttonLabel: '',
                            buttonLink: '',
                            showImage: true
                        },
                        items: [
                            Builder.checkbox(
                                { label: 'Show image', name: 'showImage' }),
                            Builder.image({ label: 'Image', name: 'image' }),
                            Builder.textField(
                                { label: 'Heading', name: 'heading' }),
                            Builder.text({ label: 'Text', name: 'text' }),
                            Builder.textField(
                                { label: 'Button label', name: 'buttonLabel' }),
                            Builder.textField(
                                { label: 'Button link', name: 'buttonLink' })
                        ]
                    })
                ]
            }
        ]
    },
    {
        id: uuid.v4(),
        name: 'Video',
        children: [
            {
                id: uuid.v4(),
                name: 'Video',
                type: 'video',
                icon: 'mdi-play-circle-outline',
                configs: {
                    heading: '',
                    cover: {},
                    align: 'medium',
                    videoLink: 'https://www.youtube.com/watch?v=dVNC64PHON4',
                    showOverlay: true,
                    style: 'image_with_play',
                    videoHeight: 'medium',
                    textSize: 'medium'
                },
                themes: [
                    Builder.image({ label: 'Cover image', name: 'cover' }),
                    Builder.select({
                        label: 'Image alignment',
                        name: 'align',
                        options: [
                            {
                                text: 'Top left',
                                value: 'left top'
                            },
                            {
                                text: 'Top center',
                                value: 'center top'
                            },
                            {
                                text: 'Top right',
                                value: 'right top'
                            },
                            {
                                text: 'Middle left',
                                value: 'left middle'
                            },
                            {
                                text: 'Middle center',
                                value: 'center middle'
                            },
                            {
                                text: 'Middle right',
                                value: 'right middle'
                            },

                            {
                                text: 'Bottom left',
                                value: 'left bottom'
                            },
                            {
                                text: 'Bottom center',
                                value: 'center bottom'
                            },
                            {
                                text: 'Bottom right',
                                value: 'right bottom'
                            }
                        ]
                    }),
                    Builder.textField({
                        label: 'Video link',
                        name: 'videoLink',
                        attrs: {
                            placeholder: 'https://www.youtube.com/watch?v=dVNC64PHON4',
                            hint: 'Accepts Youtube link',
                            'persistent-hint': true
                        }
                    }),
                    Builder.checkbox(
                        { label: 'Show overlay', name: 'showOverlay' }),
                    Builder.select({
                        label: 'Style', name: 'style', options: [
                            {
                                value: 'image_with_play',
                                text: 'Image with play button'
                            },
                            {
                                value: 'background',
                                text: 'Background video'
                            }
                        ]
                    }),
                    Builder.select({
                        label: 'Video height', name: 'videoHeight', options: [
                            {
                                value: 'small',
                                text: 'Small'
                            },
                            {
                                value: 'medium',
                                text: 'Medium'
                            },
                            {
                                value: 'large',
                                text: 'Large'
                            }
                        ]
                    }),
                    Builder.textField({ label: 'Heading', name: 'heading' }),
                    Builder.select({
                        label: 'Text size', name: 'textSize', options: [
                            {
                                value: 'medium',
                                text: 'Medium'
                            },
                            {
                                value: 'large',
                                text: 'Large'
                            }
                        ]
                    })
                ]
            }
        ]
    }
];