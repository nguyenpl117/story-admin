require('dotenv').config();
const LRU = require('lru-cache');
const colors = require('vuetify/es5/util/colors').default
const themeCache = new LRU({
    max: 10,
    maxAge: 1000 * 60 * 60, // 1 hour
});

let buildDir = '.nuxt';
if (process.env.BUILD_DIR) {
    buildDir = process.env.BUILD_DIR;
}

module.exports = {
    buildDir,
    mode: 'universal',
    /*
    ** Headers of the page
    */
    head: {
        titleTemplate: '%s - ' + process.env.npm_package_name,
        title: process.env.npm_package_name || '',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: './1.png'},
            // {
            //     rel: 'stylesheet',
            //     href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
            // }
        ],
        script: [
            { src: '/admin/tinymce/tinymce.min.js' }
        ],
    },
    /*
    ** Customize the progress-bar color
    */
    loading: {color: '#fff'},
    /*
    ** Global CSS
    */
    css: [],
    /*
    ** vuetify module configuration
    ** https://github.com/nuxt-community/vuetify-module
    */
    vuetify: {
        defaultAssets: {
            icons: 'md'
        },
        customVariables: ['~/assets/variables.scss'],
        options: {
            minifyTheme: function (css) {
                return process.env.NODE_ENV === 'production'
                    ? css.replace(/[\r\n|\r|\n]/g, '')
                    : css
            },
        },
        themeCache,
        theme: {
            // dark: true,
            themes: {
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.base
                },
                light: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.base
                }
            }
        }
    },
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        {src: '~/plugins/index'},
        '~/plugins/i18n.js',
        '~/plugins/notification.js',
        '~/plugins/service.js',
        '~/plugins/mdi-icon.js',
        {src: '~/plugins/ckeditor.js', ssr: false},
        {src: '~/plugins/codemirror.js', ssr: false},
        {src: '~/plugins/color.js', ssr: false},

        {src: '~/plugins/sortable.js', ssr: true},

        {src: '~/plugins/autonumeric-ssr.js', ssr: true},
        {src: '~/plugins/autonumeric.js', ssr: false},
    
        {src: '~/plugins/tinymce.js', ssr: false},
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [

    ],
    /*
    ** Nuxt.js modules
    */
    modules: [
        '@nuxtjs/vuetify',
        '@nuxtjs/dotenv',
        '@nuxtjs/apollo'
    ],

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        // extractCSS: process.env.NODE_ENV === 'production',
        filenames: {
            app: ({ isDev }) => isDev ? '[name].js' : '[id].[chunkhash].js',
            chunk: ({ isDev }) => isDev ? '[name].js' : '[id].[chunkhash].js',
            css: ({ isDev }) => isDev ? '[name].css' : '[name].[contenthash].css'
        },
        extend(config, ctx) {
            // Run ESLint on save
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    },
    optimizeCSS: {},

    html: {
        minify: {
            collapseBooleanAttributes: true,
            decodeEntities: true,
            minifyCSS: true,
            minifyJS: true,
            processConditionalComments: true,
            removeEmptyAttributes: true,
            removeRedundantAttributes: true,
            trimCustomFragments: true,
            useShortDoctype: true,
            collapseWhitespace: false
        }
    },

    router: {
        middleware: 'i18n',
        base: '/admin/'
    },

    // Give apollo module options
    apollo: {
        tokenExpires: 10, // optional, default: 7 (days)
        includeNodeModules: true, // optional, default: false (this includes graphql-tag for node_modules folder)
        authenticationType: 'Bearer', // optional, default: 'Bearer'


        // required
        clientConfigs: {
            default: '~/apollo/client-configs/default.js'
        }
    },
};
